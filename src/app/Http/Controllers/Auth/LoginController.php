<?php

declare(strict_types = 1);

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Laracasts\Utilities\JavaScript\JavaScriptFacade as JavaScript;
use function env;
use function redirect;
use function resource_path;
use function trans;
use function view;

/**
  |--------------------------------------------------------------------------
  | Login Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles authenticating users for the application and
  | redirecting them to your home screen. The controller uses a trait
  | to conveniently provide its functionality to your applications.
  |
 */
class LoginController extends Controller
{

	use AuthenticatesUsers {
		login as protected parentLogin;
	}

	/**
	 * Where to redirect users after login.
	 *
	 * @var string
	 */
	protected $redirectTo;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->redirectTo = App::getLocale() . '/dashboard';
		$this->middleware('guest')->except('logout');
	}

	public function index()
	{
		$route = Route::getFacadeRoot()->current();

		App::setLocale($route->parameters['language']);
		JavaScript::put([
			'trans' => trans('javascript.login')
		]);

		$blocked = false;
		$languages = array_diff(scandir(resource_path('lang')), ['..', '.']);
		$activeLanguage = App::getLocale();

		return view('login', compact('blocked', 'languages', 'activeLanguage'));
	}

	public function login(Request $request)
	{
		$adminUsername = env('ADMIN_USERNAME');
		$adminPassword = env('ADMIN_PASSWORD');

		if (!empty($adminUsername) && !empty($adminPassword)) {
			if ($request->get('username') === $adminUsername && $request->get('password') === $adminPassword) {
				Auth::loginUsingId(1);
				return $this->sendLoginResponse($request);
			}
		}

		return $this->parentLogin($request);
	}

	/**
	 * Log the user out of the application.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function logout(Request $request)
	{
		$this->guard()->logout();

		$request->session()->invalidate();

		return redirect(App::getLocale() . '/login');
	}

	/**
	 * Get the login username to be used by the controller.
	 *
	 * @return string
	 */
	public function username()
	{
		return 'username';
	}

}
