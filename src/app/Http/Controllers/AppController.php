<?php

declare(strict_types = 1);

namespace App\Http\Controllers;

use App\Http\Traits\TraitAssets;
use App\Http\Traits\TraitAuth0;
use App\Http\Traits\TraitPosts;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Laravel\Lumen\Routing\Controller;
use MatthiasMullie\Minify\CSS as MinifierCSS;
use MatthiasMullie\Minify\JS as MinifierJS;
use stdClass;
use Symfony\Component\HttpFoundation\JsonResponse;
use function dd;
use function env;
use function GuzzleHttp\json_decode;
use function GuzzleHttp\json_encode;
use function response;
use function view;

class AppController extends Controller
{

	use TraitAuth0,
	 TraitAssets,
	 TraitPosts;

	/**
	 * Basic config settings passed to template
	 *
	 * @var array 
	 */
	protected $config;

	/**
	 * Constructor
	 */
	public function __construct()
	{
		$this->config = [
			'route_api' => 'api/',
			'response' => [
				'time_real_min' => Carbon::create()->format('H:i:s'),
				'time_real' => Carbon::create()->format('Y/m/d H:i:s'),
				'time_current' => Carbon::create()->format('Y/m/d H:i:s'),
			]
		];

		switch (env('AUTH_TYPE')) {
			case 'jwt':
				$token = $this->getAuth0Token();

				$this->config['token'] = [
					'authorization' => $token->token_type . ' ' . $token->access_token
				];
				break;
			case 'csrf':
				$this->config['token'] = csrf_token();
				break;
		}

		$this->config['time_range'] = env('APP_TIMERANGE');
		$this->config['time_refresh'] = env('APP_TIMEREFRESH');
	}

	/**
	 * Index page for controller
	 *
	 * @return View
	 */
	public function index(): View
	{
		$this->config['editor'] = false;

		return $this->render();
	}

	/**
	 * Editor page for controller
	 *
	 * @return View|RedirectResponse
	 */
	public function editor(Request $request)
	{
		if ($request->isMethod('post')) {
			switch ($request->get('type')) {
				case 'object':
					return $this->postObject($request);
				case 'component':
					return $this->postComponent($request);
				case 'connector':
					return $this->postConnector($request);
			}
		}

		$this->config['editor'] = true;

		return $this->render();
	}

	/**
	 * Basic render method
	 *
	 * @return View
	 */
	public function render(): View
	{
		$objects = $this->getObjects();
		$connectorTypes = $this->getConnectorTypes();

		$config = $this->config;

		return view('app', compact('config', 'objects', 'connectorTypes'));
	}

	/**
	 * Places an entity
	 *
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function entityPlace(Request $request): JsonResponse
	{
		$uid = $request->get('uid');
		$layer = $request->get('layer');

		$randPos = rand(100, 400);
		$data = null;

		$object = DB::table('objects')
				->where('uid', $uid)
				->first();

		if (!is_null($object->data_object)) {
			$dataObject = json_decode($object->data_object);
			$data = (object) [
						'parameters' => (object) []
			];

			if (!empty($dataObject->required)) {
				foreach ($dataObject->required as $required) {
					$data->parameters->{$required} = $dataObject->parameters->{$required};
				}

				$data->required = $dataObject->required;
			}

			if (!empty($dataObject->visible)) {
				$data->visible = $dataObject->visible;
			}
		}

		$entityId = DB::table('entities')->insertGetId([
			'object_id' => $object->id,
			'layer_id' => $layer,
			'pos_x' => $randPos,
			'pos_y' => $randPos
		]);

		DB::table('history')->insert([
			'time' => Carbon::now(),
			'entity_id' => $entityId,
			'data' => $data ? json_encode($data) : null
		]);

		return $this->response();
	}

	/**
	 * Updates an entity data parameters
	 *
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function entityUpdate(Request $request): JsonResponse
	{
		$data = (object) $request->all();
		$uid = intval(preg_replace('/[^0-9.]+/', '', $data->uid));
		unset($data->uid);

		$lastEntityRow = $this->getLastEntityRow($uid);
		$modifiedData = json_decode($lastEntityRow->data);
		$modifiedData->parameters = $data;

		DB::table('history')->insert([
			'time' => Carbon::now(),
			'entity_id' => $uid,
			'data' => json_encode($modifiedData)
		]);

		return $this->response();
	}

	/**
	 * Places a connector
	 *
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function connectorPlace(Request $request): JsonResponse
	{
		$uid1 = preg_replace('/[^0-9.]+/', '', $request->get('uid1'));
		$uid2 = preg_replace('/[^0-9.]+/', '', $request->get('uid2'));

		$type = intval($request->get('type'));
		$dir1 = intval($request->get('dir1'));
		$dir2 = intval($request->get('dir2'));

		DB::table('connectors')->insert([
			'type' => $type,
			'entity_from' => $uid1,
			'entity_to' => $uid2,
			'dir_start' => $dir1,
			'dir_end' => $dir2
		]);

		return $this->response();
	}

	/**
	 * Deletes an entity
	 *
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function entityDelete(Request $request): JsonResponse
	{
		$uid = preg_replace('/[^0-9.]+/', '', $request->get('uid'));

		DB::table('entities')
				->where('id', $uid)
				->delete();

		return response()->json([], 200);
	}

	/**
	 * Deletes a connector
	 *
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function connectorDelete(Request $request): JsonResponse
	{
		$uid = preg_replace('/[^0-9.]+/', '', $request->get('uid'));

		DB::table('connectors')
				->where('id', $uid)
				->delete();

		return $this->response();
	}

	/**
	 * Updates data
	 *
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function setData(Request $request): JsonResponse
	{
		$uid = $request->get('uid');
		$x = $request->get('x');
		$y = $request->get('y');
		$entityId = intval(preg_replace('/[^0-9.]+/', '', $uid));
		$lastEntityRow = $this->getLastEntityRow($entityId);

		DB::table('history')->insert([
			'time' => Carbon::now(),
			'entity_id' => $entityId,
			'data' => $lastEntityRow ? $lastEntityRow->data : null
		]);

		DB::table('entities')
				->where('id', $entityId)
				->update(['pos_x' => $x, 'pos_y' => $y]);

		return $this->response();
	}

	/**
	 * Returns all data of entities, connectors and ios
	 *
	 * @param Request $request
	 * @return JsonResponse
	 */
	public function getData(Request $request): JsonResponse
	{
		$time = $request->get('time');

		if ($time) {
			$d = date_parse($time);
			$currentTime = Carbon::create($d['year'], $d['month'], $d['day'], $d['hour'], $d['minute'], $d['second'], env('APP_TIMEZONE'));
		} else {
			$currentTime = Carbon::now();
		}

		$layer = 1;

		$diffTime = Carbon::now()->getTimestamp() - $currentTime->getTimestamp();
		$realTime = Carbon::now();
		$newTime = Carbon::createFromTimestamp($realTime->getTimestamp() - $diffTime);

		if ($newTime->getTimestamp() > $realTime->getTimestamp()) {
			//$this->config['response']['debug'] = $newTime->getTimestamp() . '-' . $realTime->getTimestamp();
			$this->createTestData($newTime);
		}

		try {
			$connectors = DB::table('connectors')
					->select('connectors.*', 'connector_type.uid', 'connector_type.name')
					->join('connector_type', 'connectors.type', 'connector_type.id')
					->get();

			$entities = $this->getAllEntities($currentTime->format('Y-m-d H:i:s'));
			
			foreach($entities as &$entity) {
				$entityParameters = null;
				
				if ($entity->data) {
					$dataEntity = json_decode($entity->data);
					$entityParameters = isset($dataEntity->parameters) ? $dataEntity->parameters : null;
				}
				
				if ($entity->data_object && $entityParameters) {
					$dataObject = json_decode($entity->data_object);
					$objectParameters = isset($dataObject->parameters) ? $dataObject->parameters : null;
					
					if (isset($dataObject->parameters) && $objectParameters) {
						foreach($entityParameters as $entityParameter => $entityValue) {
							unset($objectParameters->{$entityParameter});
						}
						
						$dataObject->parameters = $objectParameters; //json_encode($entityParameters);
						$entity->data_object = json_encode($dataObject);
					}
				}
			}
		} catch (QueryException $exc) {
			dd($exc);
		}

		$io = (object) [];

		foreach ($connectors as $connector) {
			foreach ([$connector->entity_from, $connector->entity_to] as $entity) {
				if (!isset($io->{$entity})) {
					$io->{$entity} = (object) [
								'top' => 0,
								'left' => 0,
								'right' => 0,
								'bottom' => 0
					];
				}
			}

			$directions = [
				'dir_start' => $connector->entity_from,
				'dir_end' => $connector->entity_to,
			];

			foreach ($directions as $direction => $entity) {
				if ($connector->{$direction} === 1) {
					$io->{$entity}->left++;
				} elseif ($connector->{$direction} === 2) {
					$io->{$entity}->right++;
				} elseif ($connector->{$direction} === 4) {
					$io->{$entity}->top++;
				} elseif ($connector->{$direction} === 8) {
					$io->{$entity}->bottom++;
				}
			}
		}

		$res = [
			'connectors' => $connectors,
			'entities' => $entities,
			'io' => $io,
			'time_current' => $newTime->format('Y/m/d H:i:s'),
		];

		return $this->response($res);
	}

	/**
	 * Returns assets sucha s javascripts, stylesheets and images
	 *
	 * @param MinifierCSS $cssMinifier
	 * @param MinifierJS $jsMinifier
	 * @return JsonResponse
	 */
	public function assets(MinifierCSS $cssMinifier, MinifierJS $jsMinifier): JsonResponse
	{
		$assetsFunction = function () use ($cssMinifier, $jsMinifier) {
			return [
				'images' => $this->preloadImages(),
				'stylesheets' => $this->preloadStylesheets($cssMinifier),
				'javascripts' => $this->preloadJavascripts($jsMinifier),
			];
		};

		if (env('CACHE_ASSETS')) {
			$assets = Cache::rememberForever('assets', $assetsFunction);
		} else {
			$assets = $assetsFunction();
		}

		return $this->response($assets);
	}

	/**
	 * Returns all objects
	 *
	 * @return Collection
	 */
	public function getObjects(): Collection
	{
		return $connectors = DB::table('objects')
				->get();
	}

	/**
	 * Returns all connector types
	 *
	 * @return Collection
	 */
	public function getConnectorTypes(): Collection
	{
		return DB::table('connector_type')
						->get();
	}

	/**
	 * Returns response for all ajax calls
	 *
	 * @param array $data
	 * @return JsonResponse
	 */
	protected function response(array $data = []): JsonResponse
	{
		return response()->json(array_merge($this->config['response'], $data), 200);
	}

	/**
	 * Creates testing data afor future
	 *
	 * @param Carbon $date
	 */
	protected function createTestData(Carbon $date)
	{
		$ids = [
			83 => function () {
				return [
					['Výkon', ' %'],
					['Efektivita', ' %'],
				];
			},
			81 => function () {
				return [
					['Kapacita', ' t'],
				];
			},
			79 => function () {
				return [
					['Veľkosť', ' l'],
				];
			},
			90 => function () {
				return [
					['Attr1', ''],
					['Attrľ', ''],
				];
			},
		];

		$dateEnd = $date;
		$dateEndTimestamp = $dateEnd->getTimestamp();

		$dateStart = Carbon::now();
		$dateStartTimestamp = $dateStart->getTimestamp();

		$dateRefresh = env('APP_TIMEREFRESH');

		$entitiesStart = $this->getAllEntities($dateStart->format('Y-m-d H:i:s'));
		$entitiesEnd = $this->getAllEntities($dateEnd->format('Y-m-d H:i:s'))
				->keyBy('entity_id');

		foreach ($entitiesStart as $entity) {
			if ($entity->data && (in_array($entity->entity_id, array_keys($ids)))) {
				$data = json_decode($entity->data);
				$dateTemp = Carbon::create();
				$entityEndDate = $entitiesEnd->get($entity->entity_id) ? Carbon::createFromFormat('Y-m-d H:i:s', $entitiesEnd->get($entity->entity_id)->time) : null;

				for ($i = $dateStartTimestamp; $i < $dateEndTimestamp; $i += $dateRefresh) {
					$idsTemp = $ids[$entity->entity_id]();

					foreach ($idsTemp as $str) {
						$variable = '_' . str_slug($str[0], '_') . 'Temp';
						$data->{$variable} = ($data->{$variable} ?? 0) + (rand(-10, 10) / 10);
						$data->parameters->{$str[0]} = (intval($data->parameters->{$str[0]}) + $data->{$variable}) . $str[1];
					}

					if ($entityEndDate === null || ($entityEndDate && $i > $entityEndDate->getTimestamp())) {
						DB::table('history')->insert([
							'time' => $dateTemp->setTimestamp($i),
							'entity_id' => $entity->entity_id,
							'data' => json_encode($data)
						]);
					}
				}
			}
		}
	}

	/**
	 * Get all entities
	 *
	 * @param string $date
	 * @return Collection
	 */
	protected function getAllEntities(string $date): Collection
	{
		return DB::table('h1.entity_id', 'h1.time') // remove , 'h1.time'
						->from(DB::raw("(SELECT `history`.`entity_id`, MAX(`history`.`time`) AS `mtime`
							FROM `history`
							WHERE `history`.`time` <= ?
							GROUP BY `history`.`entity_id`) AS `h2`"))
						->join('history AS h1', function ($join) {
							$join->on('h1.entity_id', 'h2.entity_id')
							->on('h1.time', 'h2.mtime');
						})
						->join('entities', function ($join) {
							$join->on('h1.entity_id', 'entities.id');
						})
						->join('objects', function ($join) {
							$join->on('entities.object_id', 'objects.id');
						})
						//->groupBy('entity_id')
						->setBindings([$date])
						->get();
	}

	/**
	 * Returns last inserted entity row by id
	 *
	 * @param int $entityId
	 * @return stdClass
	 */
	protected function getLastEntityRow(int $entityId): stdClass
	{
		return DB::table('history')
						->select(DB::raw('history.id, history.data'))
						->where('time', '<=', Carbon::now())
						->where('entity_id', $entityId)
						->groupBy('id')
						->get()
						->last();
	}

}
