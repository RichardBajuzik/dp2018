<?php

namespace App\Http\Traits;

use Illuminate\Support\Facades\File;
use MatthiasMullie\Minify\CSS as MinifierCSS;
use MatthiasMullie\Minify\JS as MinifierJS;
use function env;
use function resource_path;

trait TraitAssets
{

	/**
	 * Returns all iamges from resource/img/objects/ fgolder
	 *
	 * @return array
	 */
	protected function preloadImages(): array
	{
		$directory = resource_path() . DIRECTORY_SEPARATOR . 'img' . DIRECTORY_SEPARATOR . 'objects';
		$files = File::allFiles($directory);
		$assets = [];

		foreach ($files as $file) {
			$key = basename(preg_replace('/\\.[^.\\s]{3,4}$/', '', (string) $file));
			$assets[$key] = asset('img/objects/' . basename((string) $file));
		}

		return $assets;
	}

	/**
	 * Returns all stylesheets in one string
	 *
	 * @param MinifierCSS $minifier
	 * @return string
	 */
	protected function preloadStylesheets(MinifierCSS $minifier): string
	{
		$directory = resource_path('css');
		$files = File::allFiles($directory);

		if (env('MINIFY_ASSETS')) {
			foreach ($files as $file) {
				$minifier->add($file->getPathName());
			}

			return $minifier->minify();
		}

		$stylesheets = '';

		foreach ($files as $file) {
			$stylesheets .= file_get_contents($file->getPathName()) . PHP_EOL;
		}

		return $stylesheets;
	}

	/**
	 * Returns all javascripts in one string
	 *
	 * @param MinifierJS $minifier
	 * @return string
	 */
	protected function preloadJavascripts(MinifierJS $minifier): string
	{
		$directory = resource_path('js');
		$files = File::allFiles($directory);

		if (env('MINIFY_ASSETS')) {
			foreach ($files as $file) {
				$minifier->add($file->getPathName());
			}

			return $minifier->minify();
		}

		$javascripts = '';

		foreach ($files as $file) {
			$javascripts .= file_get_contents($file->getPathName()) . PHP_EOL;
		}

		return $javascripts;
	}

}
