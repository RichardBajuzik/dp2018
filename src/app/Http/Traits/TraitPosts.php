<?php

namespace App\Http\Traits;

use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function redirect;
use function resource_path;

trait TraitPosts
{

	/**
	 * Returns post response of creating object
	 *
	 * @return RedirectResponse
	 */
	protected function postObject(Request $request): RedirectResponse
	{
		if (!is_null($request->file('object'))) {
			$withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $request->file('object')->getClientOriginalName());
			$path = $request->file('object')->storeAs('img/objects', $withoutExt . '.png');
			list($width, $height) = getimagesize(resource_path($path));
		} else {
			$withoutExt = 'box';
			$width = 151;
			$height = 60;
		}

		$data = null;
		$requiredList = [];
		$visibleList = [];

		if (count($request->get('attrRequired'))) {
			foreach ($request->get('attrRequired') as $key => $required) {
				$required = $request->get('attrName')[$key];

				if (!is_null($required) && $required !== '') {
					$requiredList[] = $required;
				}
			}
		}

		if (count($request->get('attrVisibility'))) {
			foreach ($request->get('attrVisibility') as $key => $visible) {
				$visible = $request->get('attrName')[$key];

				if (!is_null($visible) && $visible !== '') {
					$visibleList[] = $visible;
				}
			}
		}

		if (count($request->get('attrName'))) {
			foreach ($request->get('attrName') as $key => $name) {
				$value = $request->get('attrValue')[$key];

				if (!is_null($name) && $name !== '') {
					if (!in_array($name, $requiredList) && !in_array($name, $visibleList) && ($value === '' || is_null($value))) {
						continue;
					}

					if ($data === null) {
						$data = (object) [
									'visible' => $visibleList,
									'required' => $requiredList,
									'parameters' => (object) []
						];
					}

					$data->parameters->{$name} = $value;
				}
			}
		}

		DB::table('objects')->insert([
			'name' => empty($request->name) ? 'Component' : $request->name,
			'uid' => $withoutExt,
			'width' => $width,
			'height' => $height,
			'data_object' => $data ? json_encode($data) : null
		]);

		return redirect()->route('route.editor');
	}

	/**
	 * Returns post response of editing components
	 *
	 * @return RedirectResponse
	 */
	protected function postComponent(Request $request): RedirectResponse
	{
		$parameters = null;
		$uid = intval(preg_replace('/[^0-9.]+/', '', $request->get('uid')));

		$object = DB::table('entities')
				->join('objects', 'entities.object_id', 'objects.id')
				->where('entities.id', $uid)
				->first();

		$dataObject = isset($object->data_object) ? json_decode($object->data_object) : [];
		$requiredList = !empty($dataObject->required) ? $dataObject->required : [];

		if (count($request->get('attrName'))) {
			foreach ($request->get('attrName') as $key => $name) {
				$value = $request->get('attrValue')[$key];

				if (!is_null($name) && $name !== '') {
					if (!in_array($name, $requiredList) && ($value === '' || is_null($value))) {
						continue;
					}

					if ($parameters === null) {
						$parameters = (object) [];
					}

					$parameters->{$name} = $value;
				}
			}
		}

		$lastEntity = $this->getLastEntityRow($uid);
		$encodedOldData = json_decode($lastEntity->data ?? '{}');
		$encodedOldData->parameters = $parameters;
		$encodedOldData->required = $requiredList;
		$encodedOldData->visible = [];

		if (count($request->get('attrVisibility'))) {
			foreach ($request->get('attrVisibility') as $key => $visible) {
				$visible = $request->get('attrName')[$key];

				if (!is_null($visible) && $visible !== '') {
					$encodedOldData->visible[] = $visible;
				}
			}
		}

		DB::table('history')->insert([
			'time' => Carbon::now(),
			'entity_id' => $uid,
			'data' => $parameters ? json_encode($encodedOldData) : null
		]);

		return redirect()->route('route.editor');
	}

	/**
	 * Returns post response of editing components
	 *
	 * @return RedirectResponse
	 */
	protected function postConnector(Request $request): RedirectResponse
	{
		$parameters = null;
		$id = intval($request->get('id'));

		foreach ($request->get('attrName') as $key => $name) {
			$value = $request->get('attrValue')[$key];
			if (!is_null($value) && $value !== '' && !is_null($name) && $name !== '') {
				if ($parameters === null) {
					$parameters = (object) [];
				}

				$parameters->{$name} = $value;
			}
		}

		DB::table('connectors')
				->where('id', $id)
				->update(['data' => $parameters ? json_encode($parameters) : null]);

		return redirect()->route('route.editor');
	}

}
