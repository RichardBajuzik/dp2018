<?php

namespace App\Http\Traits;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use stdClass;
use function dd;
use function env;

trait TraitAuth0
{

	/**
	 * Returns token from Auth0 online service
	 *
	 * @return stdClass
	 */
	protected function getAuth0Token(): stdClass
	{
		$tokenFunction = function () {
			$curl = curl_init();

			curl_setopt_array($curl, [
				CURLOPT_URL => env('AUTH0_TOKEN_URL'),
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => "{\"client_id\":\"" . env('AUTH0_ID') . "\",\"client_secret\":\"" . env('AUTH0_SECRET') . "\",\"audience\":\"" . env('AUTH0_API_AUDIENCE') . "\",\"grant_type\":\"client_credentials\"}",
				CURLOPT_HTTPHEADER => [
					"content-type: application/json"
				],
			]);

			$response = curl_exec($curl);
			$err = curl_error($curl);

			if ($err !== '' && App::environment() !== 'production') {
				dd($response);
			}

			if ($response === false) {
				echo 'FALSE';
				dd($err);
			}

			curl_close($curl);

			return json_decode($response);
		};

		if (env('CACHE_TOKEN')) {
			return Cache::remember('token', 5, $tokenFunction);
		}

		return $tokenFunction();
	}

}
