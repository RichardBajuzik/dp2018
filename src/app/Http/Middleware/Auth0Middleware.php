<?php

namespace App\Http\Middleware;

use Auth0\SDK\Exception\CoreException;
use Auth0\SDK\JWTVerifier;
use Closure;
use Illuminate\Http\Request;
use function response;

class Auth0Middleware
{

	/**
	 * Run the request filter.
	 *
	 * @param  Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if (!$request->hasHeader('Authorization')) {
			return response()->json('Authorization Header not found', 401);
		}

		$token = $request->bearerToken();

		if ($request->header('Authorization') == null || $token == null) {
			return response()->json('No token provided', 401);
		}

		$this->retrieveAndValidateToken($token);

		return $next($request);
	}

	public function retrieveAndValidateToken($token)
	{
		try {
			$verifier = new JWTVerifier([
				'supported_algs' => ['RS256'],
				'valid_audiences' => [env('AUTH0_API_AUDIENCE')],
				'authorized_iss' => [env('AUTH0_DOMAIN')]
			]);

			$decoded = $verifier->verifyAndDecode($token);
		} catch (CoreException $e) {
			throw $e;
		};
	}

}
