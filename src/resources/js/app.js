/* global ajaxGet, ajaxPost, app, ajaxPut, images, ajaxDelete */

var App = function () {
	'use strict';

	/*********************
	 * browser detection *
	 *********************/

	var ie = document.all;
	var nn6 = document.getElementById && !document.all;
	var sideBarWidth = 100;
	var sideBarWidthMin = 0;
	var routeApi = app.route_api;
	var editMode = false;
	var placingConnector = false;
	var connectorType;
	var firstConnector;
	var modalBox = document.getElementById("modalBox");
	var appConfig = app;
	var slider;
	var listenerCount = 0;

	var realTime;
	var currentTime = parseTime(appConfig.response.time_current);

	var timeUpdated = 0;
	var layer = 1;

	/*****************
	 * drag and drop *
	 *****************/

	var isdrag = false;					// this flag indicates that the mouse movement is actually a drag.
	var mouseStartX, mouseStartY;		// mouse position when drag starts
	var mouseEndX, mouseEndY;		// mouse position when drag ends
	var elementStartX, elementStartY;	// element position when drag starts

	/**
	 * the html element being dragged.
	 */
	var elementToMove;

	/**
	 * an array containing the blocks being dragged. This is used to notify them of move.
	 */
	var blocksToMove;

	/**
	 * this variable stores the orginal z-index of the object being dragged in order
	 * to restore it upon drop.
	 */
	var originalZIndex;

	/**
	 * an array containing bounds to be respected while dragging elements,
	 * these bounds are left, top, left + width, top + height of the parent element.
	 */
	var bounds = new Array(4);

	var blocksToMoveScanner = new DocumentScanner(new BlocksToMoveVisitor(), true);

	/*************
	 * Constants *
	 *************/
	var LEFT = 1;
	var RIGHT = 2;
	var UP = 4;
	var DOWN = 8;
	var HORIZONTAL = LEFT + RIGHT;
	var VERTICAL = UP + DOWN;
	var AUTO = HORIZONTAL + VERTICAL;

	var START = 0;
	var END = 1;
	var SCROLLBARS_WIDTH = 18;

	/**************
	 * Inspectors *
	 **************/

	var inspectors = new Array();
	var strategies = new Array();
	var canvases = new Array();

	document.onmousedown = startDrag;
	document.onmouseup = stopDrag;
	document.addEventListener('contextmenu', event => event.preventDefault());

	var appAjaxGet = function () {
		var mainArguments = Array.prototype.slice.call(arguments);
		mainArguments.push(appConfig.token);

		return ajaxGet.apply(null, mainArguments);
	};

	var appAjaxPost = function () {
		var mainArguments = Array.prototype.slice.call(arguments);
		mainArguments.push(appConfig.token);

		return ajaxPost.apply(null, mainArguments);
	};

	var appAjaxPut = function () {
		var mainArguments = Array.prototype.slice.call(arguments);
		mainArguments.push(appConfig.token);

		return ajaxPut.apply(null, mainArguments);
	};

	var appAjaxDelete = function () {
		var mainArguments = Array.prototype.slice.call(arguments);
		mainArguments.push(appConfig.token);

		return ajaxDelete.apply(null, mainArguments);
	};

	var querySelector = document.querySelector.bind(document);
	var nav = document.querySelector('.vertical_nav');
	var wrapper = document.querySelector('.wrapper');
	var menu = document.getElementById("js-menu");
	var subnavs = menu.querySelectorAll('.menu--item__has_sub_menu');

	var extend = function (a, b) {
		for (var key in b) {
			if (b.hasOwnProperty(key)) {
				a[key] = b[key];
			}
		}
		return a;
	};

	/**
	 * Fire an event handler to the specified node. Event handlers can detect that the event was fired programatically
	 * by testing for a 'synthetic=true' property on the event object
	 *
	 * @param {HTMLNode} node The node to fire the event handler on.
	 * @param {String} eventName The name of the event without the "on" (e.g., "focus")
	 */
	var fireEvent = function (node, eventName) {
		// Make sure we use the ownerDocument from the provided node to avoid cross-window problems
		var doc;
		if (node.ownerDocument) {
			doc = node.ownerDocument;
		} else if (node.nodeType === 9) {
			// the node may be the document itself, nodeType 9 = DOCUMENT_NODE
			doc = node;
		} else {
			throw new Error("Invalid node passed to fireEvent: " + node.id);
		}

		if (node.dispatchEvent) {
			// Gecko-style approach (now the standard) takes more work
			var eventClass = "";

			// Different events have different event classes.
			// If this switch statement can't map an eventName to an eventClass,
			// the event firing is going to fail.
			switch (eventName) {
				case "click": // Dispatching of 'click' appears to not work correctly in Safari. Use 'mousedown' or 'mouseup' instead.
				case "mousedown":
				case "mouseup":
					eventClass = "MouseEvents";
					break;

				case "focus":
				case "change":
				case "blur":
				case "select":
					eventClass = "HTMLEvents";
					break;

				default:
					throw "fireEvent: Couldn't find an event class for event '" + eventName + "'.";
					break;
			}
			var event = doc.createEvent(eventClass);
			event.initEvent(eventName, true, true); // All events created as bubbling and cancelable.

			event.synthetic = true; // allow detection of synthetic events
			// The second parameter says go ahead with the default action
			node.dispatchEvent(event, true);
		} else if (node.fireEvent) {
			// IE-old school style, you can drop this if you don't need to support IE8 and lower
			var event = doc.createEventObject();
			event.synthetic = true; // allow detection of synthetic events
			node.fireEvent("on" + eventName, event);
		}
	};

	var calculateSideBarOffset = function () {
		if (nav.className.indexOf('vertical_nav__minify') === -1) {
			return -sideBarWidth * 2;
		}

		return -sideBarWidthMin;
	};

	/****************************************************
	 * This class is a scanner for the visitor pattern. *
	 ****************************************************/

	/**
	 * Constructor, parameters are:
	 * visitor: the visitor implementation, it must be a class with a visit(element) method.
	 * scanElementsOnly: a flag telling whether to scan html elements only or all html nodes.
	 *
	 * @param {type} visitor
	 * @param {type} scanElementsOnly
	 * @returns {js-graph-itApp.DocumentScanner}
	 */
	function DocumentScanner(visitor, scanElementsOnly)
	{
		this.visitor = visitor;
		this.scanElementsOnly = scanElementsOnly;

		/**
		 * Scans the element
		 *
		 * @param {type} element
		 * @returns {undefined}
		 */
		this.scan = function (element)
		{
			var i;
			if (this.visitor.visit(element))
			{
				// visit child elements
				var children = element.childNodes;
				for (i = 0; i < children.length; i++)
				{
					if (!this.scanElementsOnly || children[i].nodeType === 1)
					{
						this.scan(children[i]);
					}
				}
			}
		};
	}

	/**
	 * this visitor is used to find blocks nested in the element being moved.
	 */
	function BlocksToMoveVisitor()
	{
		this.visit = function (element)
		{
			if (isBlock(element))
			{
				blocksToMove.push(findBlock(element.id));
				return false;
			} else {
				return true;
			}
		};
	}

	function movemouse(e)
	{
		if (isdrag)
		{
			var currentMouseX = nn6 ? e.clientX : event.clientX;
			var currentMouseY = nn6 ? e.clientY : event.clientY;
			var newElementX = elementStartX + currentMouseX - mouseStartX;
			var newElementY = elementStartY + currentMouseY - mouseStartY;

			// check bounds
			// note: the "-1" and "+1" is to avoid borders overlap
			if (newElementX < bounds[0]) {
				newElementX = bounds[0] + 1;
			}
			if (newElementX + elementToMove.offsetWidth > bounds[2]) {
				newElementX = bounds[2] - elementToMove.offsetWidth - 1;
			}
			if (newElementY < bounds[1]) {
				newElementY = bounds[1] + 1;
			}
			if (newElementY + elementToMove.offsetHeight > bounds[3]) {
				newElementY = bounds[3] - elementToMove.offsetHeight - 1;
			}

			// move element
			elementToMove.style.left = newElementX + 'px';
			elementToMove.style.top = newElementY + 'px';

			// elementToMove.style.left = newElementX / elementToMove.parentNode.offsetWidth * 100 + '%';
			// elementToMove.style.top  = newElementY / elementToMove.parentNode.offsetHeight * 100 + '%';

			elementToMove.style.right = null;
			elementToMove.style.bottom = null;

			var i;
			for (i = 0; i < blocksToMove.length; i++)
			{
				blocksToMove[i].onMove();
			}
			return false;
		}
	}

	function rightClick(e) {
		if (appConfig.editor === false) {
			if (hasClass(e.target, 'block-image')) {
				//console.log(2)
			}

			return;
		}

		if (placingConnector) {
			return;
		}

		if (hasClass(e.target, 'block-image')) {
			appAjaxDelete(routeApi + 'entity', {
				uid: e.target.offsetParent.id
			}, function () {
				refreshCanvases();
			});
		} else if (hasClass(e.target, 'connector')) {
			appAjaxDelete(routeApi + 'connector', {
				uid: parseInt(e.target.attributes.rel.value)
			}, function () {
				refreshCanvases();
			});
		}
	}

	function placingClick(e) {
		if (!hasClass(e.target, 'marked') && hasClass(e.target, 'connector-marker')) {
			if (firstConnector) {
				var secondConnector = e.target;

				appAjaxPost(routeApi + 'connector', {
					uid1: firstConnector.parentNode.id,
					uid2: secondConnector.parentNode.id,
					type: connectorType,
					dir1: parseInt(firstConnector.attributes.dir.value),
					dir2: parseInt(secondConnector.attributes.dir.value)
				}, function () {
					refreshCanvases();
				});


				document.body.classList.remove("placing-connector");
				placingConnector = false;
				firstConnector = null;
				return;
			}

			firstConnector = e.target;
			e.target.classList.add('marked');
		}

	}

	function handleCheckBoxes() {
		Array.from(document.getElementsByClassName("checkboxHandler")).forEach(function (element) {
			element.value = element.checked ? '1' : '';

			if (!hasClass(element, 'handled')) {
				element.addEventListener('change', function () {
					this.value = this.checked ? '1' : '';
				});
				element.classList.add('handled');
			}
		});
	}

	function updateData(el) {
		var data = JSON.parse(decodeURIComponent(el.attributes.data.value));
		var uid = el.attributes.uid.value;
		var parametersData = {};

		for (var i in data) {
			parametersData[i] = document.getElementById('param' + i).value;
		}

		parametersData.uid = uid;
		appAjaxPut(routeApi + 'entity', parametersData, function () {
			hideModal();
		});
	}

	/**
	 * finds the innermost draggable element starting from the one that generated the event "e"
	 * (i.e.: the html element under mouse pointer), then setup the document's onmousemove function to
	 * move the element around.
	 *
	 * @param {type} e
	 * @returns {undefined|Boolean}
	 */
	function startDrag(e)
	{
		if (hasClass(e.target, 'modal-box-wrapper')) {
			hideModal();
			return;
		}

		if (appConfig.editor === false) {
			if (e.buttons === 1) {
				var data;

				if (hasClass(e.target, 'block-image')) {
					showModal("modalContainerInfo");
				}

				if (hasClass(e.target, 'block-image')) {
					data = JSON.parse(decodeURIComponent(e.target.parentNode.attributes.data.value)) || {};
					var dataObject = JSON.parse(decodeURIComponent(e.target.parentNode.attributes.dataobject.value)) || {};
					var k = images[e.target.attributes.alt.value].outerHTML;
					k += '<form id="entityForm" name="entityForm" method="post">';

					if (e.target.parentNode.attributes.alt) {
						k += '<h3>' + e.target.parentNode.attributes.alt.value + '</h3>';
					}

					var display = true;

					if (!Object.keys(data).length || !Object.keys(data.parameters).length) {
						if (!Object.keys(dataObject).length || !Object.keys(dataObject.parameters).length) {
							display = false;
						}
					}

					if (display) {
						k += '<form id="entityForm" name="entityForm" method="post">';
						k += '<center><table cellpadding="3" cellspacing="3" border="0" width="80%">';
						k += '<tbody>';

						var showConstants = false;
						var requiredList;

						if (Object.keys(dataObject).length && Object.keys(dataObject.parameters).length) {
							requiredList = dataObject.required ? (Object.keys(dataObject.required).length ? dataObject.required : []) : [];
							
							for (var i in dataObject.parameters) {
								if (requiredList.indexOf(i) === -1) {
									showConstants = true;
								}
							}
						}

						if (showConstants) {
							k += '<tr>';
							k += '<td colspan="2"><strong>Constants:</strong></td>';
							k += '</tr>';

							for (var i in dataObject.parameters) {
								if (requiredList.indexOf(i) === -1) {
									k += '<tr>';
									k += '<td>' + i + ':</td>';
									k += '<td><strong>' + formatNumber(dataObject.parameters[i]) + '</strong></td>';
									k += '</tr>';
								}
							}
						}

						if (Object.keys(data).length && Object.keys(data.parameters).length) {
							k += '<tr class="tr-parameters">';
							k += '<td colspan="2"><strong>Parameters:</strong></td>';
							k += '</tr>';

							for (var i in data.parameters) {
								k += '<tr>';
								k += '<td>' + i + '</td>';
								k += '<td><input class="updateDataInput" id="param' + i + '" type="text" value="' + formatNumber(data.parameters[i]) + '" /></td>';
								k += '</tr>';
							}

							if (Object.keys(data.parameters).length) {
								k += '<tr>';
								k += '<td></td>';
								k += '<td><input uid="' + e.target.parentNode.id + '" data="' + encodeURIComponent(JSON.stringify(data.parameters)) + '" onclick="App.updateData(this);" class="updateDataBtn" type="button" value="Update data" /></td>';
								k += '</tr>';
							}
						}

						k += '</tbody>';
						k += '</table></center>';
						k += '</form>';
					}

					var containerInfo = document.getElementById('modalContainerInfo');
					containerInfo.innerHTML = k;
				}

				return;
			}
		}


		if (placingConnector) {
			placingClick(e);
			return;
		}

		if (e.buttons === 2) {
			rightClick(e);
			return;
		}

		var eventSource = nn6 ? e.target : event.srcElement;

		if (eventSource.tagName === 'HTML') {
			return;
		}

		if (hasClass(eventSource, "connector"))
		{
			var data = e.target.attributes.data.value;
			var dataConnector = JSON.parse(decodeURIComponent(data)) || {};

			showModal('modalContainerConnector');

			if (Object.keys(dataConnector).length) {
				for (var i in dataConnector) {
					var input = document.createElement("tr");
					input.className = "attribute-connector-new";
					input.innerHTML += '<td colspan="2">' +
							'<input name="attrName[]" value="' + i + '" placeholder="Attribute name" type="text">&nbsp;&nbsp;' +
							'<input name="attrValue[]" value="' + dataConnector[i] + '" placeholder="Attribute value" type="text">' +
							'</td>';

					var _parentN = document.getElementsByClassName('attribute-connector-new');
					var _parentNode = _parentN[_parentN.length - 1];

					_parentNode.parentNode.insertBefore(input, _parentNode);
				}
			}

			var connectorId = document.getElementById('connectorId');
			connectorId.value = e.target.attributes.rel.value;
			return;
		}

		if (hasClass(eventSource, "btn-object-add"))
		{
			var input = document.createElement("tr");
			var _parentN = document.getElementsByClassName('attribute-new');
			var _parentNode = _parentN[_parentN.length - 1];
			var rel = parseInt(_parentNode.attributes.rel.value) + 1;

			input.className = "attribute-new";
			input.innerHTML += '<td>' +
					'<input name="attrName[' + rel + ']" placeholder="Attribute name" type="text">' +
					'</td><td>' +
					'<input name="attrValue[' + rel + ']" placeholder="Attribute value" type="text">' +
					'</td><td>' +
					'<input name="attrRequired[' + rel + ']" class="checkboxHandler" value="0" title="Required attribute" type="checkbox">' +
					'&nbsp;&nbsp;' +
					'<input name="attrVisibility[' + rel + ']" class="checkboxHandler" value="0" title="Toggle tooltip" type="checkbox">' +
					'</td>';

			_parentNode.parentNode.insertBefore(input, _parentNode.nextSibling);
			input.setAttribute('rel', rel);

			handleCheckBoxes();
			return;
		}

		if (hasClass(eventSource, "btn-component-add"))
		{
			var input = document.createElement("tr");
			var _parentN = document.getElementsByClassName('attribute-param-new');
			var _parentNode = _parentN[_parentN.length - 2] || _parentN[_parentN.length - 1];
			var rel = parseInt(_parentNode.attributes.rel.value) + 1;

			input.className = "attribute-param-new";
			input.innerHTML += '<td>' +
					'<input name="attrName[' + rel + ']" placeholder="Attribute name" type="text">' +
					'</td><td>' +
					'<input name="attrValue[' + rel + ']" placeholder="Attribute value" type="text">' +
					'</td><td>' +
					'<input name="attrVisibility[' + rel + ']" class="checkboxHandler" value="0" title="Toggle tooltip" type="checkbox">' +
					'</td>';

			var _parentN = document.getElementsByClassName('attribute-param-new');
			var _parentNode = _parentN[_parentN.length - 1];
			_parentNode.parentNode.insertBefore(input, _parentNode.nextSibling);
			return;
		}

		if (hasClass(eventSource, "btn-connector-add"))
		{
			var input = document.createElement("tr");
			input.className = "attribute-connector-new";
			input.innerHTML += '<td colspan="2">' +
					'<input name="attrName[]" placeholder="Attribute name" type="text">&nbsp;&nbsp;' +
					'<input name="attrValue[]" placeholder="Attribute value" type="text">' +
					'</td>';

			var _parentN = document.getElementsByClassName('attribute-connector-new');
			var _parentNode = _parentN[_parentN.length - 1];
			_parentNode.parentNode.insertBefore(input, _parentNode.nextSibling);
			return;
		}

		while (eventSource !== document.body && !hasClass(eventSource, "draggable"))
		{
			eventSource = nn6 ? eventSource.parentNode : eventSource.parentElement;
		}

		// if a draggable element was found, calculate its actual position
		if (hasClass(eventSource, "draggable"))
		{
			isdrag = true;
			elementToMove = eventSource;

			// set absolute positioning on the element		
			elementToMove.style.position = "absolute";
			elementToMove.classList.add('dragging');

			// calculate start point
			elementStartX = elementToMove.offsetLeft;
			elementStartY = elementToMove.offsetTop;

			// calculate mouse start point
			mouseStartX = nn6 ? e.clientX : event.clientX;
			mouseStartY = nn6 ? e.clientY : event.clientY;

			// calculate bounds as left, top, width, height of the parent element
			if (getStyle(elementToMove.parentNode, "position") === 'absolute')
			{
				bounds[0] = 0;
				bounds[1] = 0;
			} else {
				bounds[0] = calculateOffsetLeft(elementToMove.parentNode);
				bounds[1] = calculateOffsetTop(elementToMove.parentNode);
			}

			bounds[2] = bounds[0] + elementToMove.parentNode.offsetWidth;
			bounds[3] = bounds[1] + elementToMove.parentNode.offsetHeight;

			// either find the block related to the dragging element to call its onMove method
			blocksToMove = new Array();

			blocksToMoveScanner.scan(eventSource);
			document.onmousemove = movemouse;

			originalZIndex = getStyle(elementToMove, "z-index");
			elementToMove.style.zIndex = "25";

			return false;
		}
	}

	function stopDrag(e)
	{
		isdrag = false;
		if (elementToMove) {
			elementToMove.style.zIndex = originalZIndex;
			elementToMove.classList.remove('dragging');
		}

		// calculate mouse start point
		if (e) {
			mouseEndX = nn6 ? e.clientX : event.clientX;
			mouseEndY = nn6 ? e.clientY : event.clientY;

			if (mouseEndX === mouseStartX && mouseEndY === mouseStartY) {
				var dataEntity = JSON.parse(decodeURIComponent(e.target.parentNode.attributes.data.value)) || {};
				var dataObject = JSON.parse(decodeURIComponent(e.target.parentNode.attributes.dataobject.value)) || {};

				dataEntity = extend({
					parameters: {},
					visible: []
				}, dataEntity);

				dataObject = extend({
					parameters: {},
					visible: []
				}, dataObject);

				showModal("modalContainerComponent");
				document.getElementById('componentName').innerHTML = e.target.parentNode.attributes.alt.value;

				var _parentDataConstants = document.getElementsByClassName('attribute-param-new-constants')[0];
				var _parentDataConstantsText = '<td>';
				var renderConstants = false;
				var requiredList;

				if (Object.keys(dataObject.parameters).length) {
					requiredList = dataObject.required || [];

					if (Object.keys(dataObject.parameters).length) {
						for (var i in dataObject.parameters) {
							if (requiredList.indexOf(i) === -1) {
								renderConstants = true;
								break;
							}
						}
					}

					if (renderConstants) {
						_parentDataConstantsText += '<strong>Constants:</strong>';

						for (var i in dataObject.parameters) {
							if (requiredList.indexOf(i) === -1) {
								_parentDataConstantsText += '<br />' + i + ':';
							}
						}

						_parentDataConstantsText += '<br /><br />';
					}
				}
				_parentDataConstantsText += '<strong>Attributes:</strong></td><td>';

				if (Object.keys(dataEntity.parameters).length) {
					if (renderConstants) {
						for (var i in dataObject.parameters) {
							if (requiredList.indexOf(i) === -1) {
								_parentDataConstantsText += '<br /><strong>' + dataObject.parameters[i] + '</strong>';
							}
						}
						_parentDataConstantsText += '<br /><br /><br />';
					}

					_parentDataConstantsText += '</td>';
					_parentDataConstants.innerHTML = _parentDataConstantsText;

					for (var i in dataEntity.parameters) {
						var _parentN = document.getElementsByClassName('attribute-param-new');
						var _parentNode = _parentN[_parentN.length - 1];
						var rel = parseInt((_parentN[_parentN.length - 2] || _parentN[_parentN.length - 1]).attributes.rel.value) + 1;
						var input = document.createElement("tr");
						var visibility = dataEntity.visible.indexOf(i) !== -1 ? ' value="1" checked="checked"' : ' value="0"';

						input.className = "attribute-param-new";
						input.innerHTML += '<td>' +
								(dataEntity.required && dataEntity.required.indexOf(i) !== -1 ? '<input name="attrName[' + rel + ']" value="' + i + '" type="hidden">' + i :
										'<input name="attrName[' + rel + ']" value="' + i + '" placeholder="Attribute name" type="text">') +
								'</td><td>' +
								'<input name="attrValue[' + rel + ']" value="' + dataEntity.parameters[i] + '" placeholder="Attribute value" type="text">' +
								'</td><td>' +
								'<input name="attrVisibility[' + rel + ']" class="checkboxHandler" ' + visibility + ' title="Toggle tooltip" type="checkbox">' +
								'</td>';

						_parentNode.parentNode.insertBefore(input, _parentNode);
						input.setAttribute('rel', rel);
					}
				} else {
					if (renderConstants) {
						for (var i in dataObject.parameters) {
							if (requiredList.indexOf(i) === -1) {
								_parentDataConstantsText += '<br /><strong>' + dataObject.parameters[i] + '</strong>';
							}
						}

						_parentDataConstantsText += '<br /><br /><br />';
					}
					_parentDataConstantsText += '</td>';
					_parentDataConstants.innerHTML = _parentDataConstantsText;
				}

				var componentUid = document.getElementById('componentUid');
				componentUid.value = e.target.parentNode.id;

				blocksToMove = new Array();
				elementToMove = null;
				document.onmousemove = null;
				return;
			}
		}

		if (blocksToMove) {
			for (var i = 0; i < blocksToMove.length; i++)
			{
				appAjaxPut(routeApi + 'data', {
					uid: blocksToMove[i].id,
					x: blocksToMove[i].currentLeft,
					y: blocksToMove[i].currentTop
				}, function (data) {
					updateTime(data);
				});
			}
		}

		blocksToMove = new Array();
		elementToMove = null;
		document.onmousemove = null;
	}

	/**
	 * The canvas class.
	 * This class is built on a div html element.
	 *
	 * @param {type} htmlElement
	 * @returns {js-graph-itApp.Canvas}
	 */
	function Canvas(htmlElement)
	{
		/*
		 * initialization
		 */

		// create the inner div element
		this.innerDiv = document.createElement("div");
		this.initializedOnce = false;

		this.reset = function () {
			this.id = htmlElement.id;
			this.htmlElement = htmlElement;
			this.blocks = new Array();
			this.connectors = new Array();
			this.offsetLeft = calculateOffsetLeft(this.htmlElement);
			this.offsetTop = calculateOffsetTop(this.htmlElement);

			this.width;
			this.height;

			/*
			 * Inspector registration
			 */
			inspectors = new Array();
			inspectors.push(new ConnectorEndsInspector());
			inspectors.push(new ConnectorLabelsInspector());

			strategies = new Array();

			strategies[0] = function (connector) {
				return new HorizontalSStrategy(connector);
			};
			strategies[1] = function (connector) {
				return new VerticalSStrategy(connector);
			};
			strategies[2] = function (connector) {
				return new HorizontalLStrategy(connector);
			};
			strategies[3] = function (connector) {
				return new VerticalLStrategy(connector);
			};
			strategies[4] = function (connector) {
				return new HorizontalCStrategy(connector, LEFT);
			};
			strategies[5] = function (connector) {
				return new HorizontalCStrategy(connector, RIGHT);
			};
			strategies[6] = function (connector) {
				return new VerticalCStrategy(connector, UP);
			};
			strategies[7] = function (connector) {
				return new VerticalCStrategy(connector, DOWN);
			};

			blocksToMoveScanner = new DocumentScanner(new BlocksToMoveVisitor(), true);
			bounds = new Array(4);
			blocksToMove = new Array();
			placingConnector = false;
			connectorType = null;
			firstConnector = null;
			stopDrag();
		};

		this.initCanvas = function ()
		{
			// setup the inner div
			var children = this.htmlElement.childNodes;
			var i;
			var el;
			var n = children.length;
			for (i = 0; i < n; i++)
			{
				el = children[0];
				this.htmlElement.removeChild(el);
				this.innerDiv.appendChild(el);
				if (el.style) {
					el.style.zIndex = "25";
				}
			}
			//if (!this.initializedOnce) {
			this.htmlElement.appendChild(this.innerDiv);
			this.initializedOnce = true;
			//}


			this.innerDiv.id = this.id + "_innerDiv";
			this.innerDiv.style.border = "none";
			this.innerDiv.style.padding = "0px";
			this.innerDiv.style.margin = "0px";
			this.innerDiv.style.position = "absolute";
			this.innerDiv.style.top = "0px";
			this.innerDiv.style.left = "0px";
			this.width = 0;
			this.height = 0;
			this.offsetLeft = calculateOffsetLeft(this.innerDiv);
			this.offsetTop = calculateOffsetTop(this.innerDiv);

			// inspect canvas children to identify first level blocks
			new DocumentScanner(this, true).scan(this.htmlElement);

			// now this.width and this.height are populated with minimum values needed for the inner
			// blocks to fit, add 2 to avoid border overlap;
			this.height += 2;
			this.width += 2;

			var visibleWidth = this.htmlElement.offsetWidth - 2; // - 2 is to avoid border overlap
			var visibleHeight = this.htmlElement.offsetHeight - 2; // - 2 is to avoid border overlap

			// consider the scrollbars width calculating the inner div size
			if (this.height > visibleHeight)
				visibleWidth -= SCROLLBARS_WIDTH;
			if (this.width > visibleWidth)
				visibleHeight -= SCROLLBARS_WIDTH;

			this.height = Math.max(this.height, visibleHeight);
			this.width = Math.max(this.width, visibleWidth);

			this.innerDiv.style.width = this.width + "px";
			this.innerDiv.style.height = this.height + "px";

			// init connectors
			for (i = 0; i < this.connectors.length; i++)
			{
				this.connectors[i].initConnector();
			}

			this.initAppStuf();
		};

		this.initAppStuf = function () {

			var init = function () {
				blocksToMove = new Array();

				for (var i in canvases)
				{
					for (var u in canvases[i].blocks)
					{
						blocksToMove.push(findBlock(canvases[i].blocks[u].id));
					}
				}

				for (var i = 0; i < blocksToMove.length; i++)
				{
					blocksToMove[i].onMove();
				}

				blocksToMove = new Array();
			};

			init();
			setTimeout(init, 150);
		};

		this.visit = function (element)
		{
			if (element === this.htmlElement) {
				return true;
			}

			// check the element dimensions against the acutal size of the canvas
			this.width = Math.max(this.width, calculateOffsetLeft(element) - this.offsetLeft + element.offsetWidth);
			this.height = Math.max(this.height, calculateOffsetTop(element) - this.offsetTop + element.offsetHeight);

			if (isBlock(element))
			{
				// block found initialize it
				var newBlock = new Block(element, this);
				newBlock.initBlock();
				this.blocks.push(newBlock);

				return false;
			} else if (isConnector(element))
			{
				// connector found, just create it, source or destination blocks may not 
				// have been initialized yet
				var newConnector = new Connector(element, this);
				this.connectors.push(newConnector);

				return false;
			} else
			{
				// continue searching nested elements
				return true;
			}
		};

		/*
		 * methods
		 */
		this.print = function ()
		{
			var output = '<ul><legend>canvas: ' + this.id + '</legend>';
			var i;
			for (i = 0; i < this.blocks.length; i++)
			{
				output += '<li>';
				output += this.blocks[i].print();
				output += '</li>';
			}
			output += '</ul>';

			return output;
		};

		/*
		 * This function searches for a nested block with a given id
		 */
		this.findBlock = function (blockId)
		{
			var result;
			var i;
			for (i = 0; i < this.blocks.length && !result; i++)
			{
				result = this.blocks[i].findBlock(blockId);
			}

			return result;
		};

		this.toString = function ()
		{
			return 'canvas: ' + this.id;
		};
	}

	/*
	 * Block class
	 */
	function Block(htmlElement, canvas)
	{
		/*
		 * initialization
		 */

		this.canvas = canvas;
		this.htmlElement = htmlElement;
		this.id = htmlElement.id;
		this.blocks = new Array();
		this.moveListeners = new Array();

		if (this.id === 'description2_out1') {
			var merda = 0;
		}

		this.currentTop = calculateOffsetTop(this.htmlElement) - this.canvas.offsetTop;
		this.currentLeft = calculateOffsetLeft(this.htmlElement) - this.canvas.offsetLeft;

		this.visit = function (element)
		{
			if (element === this.htmlElement)
			{
				// exclude itself
				return true;
			}

			if (isBlock(element))
			{
				var innerBlock = new Block(element, this.canvas);
				innerBlock.initBlock();
				this.blocks.push(innerBlock);
				this.moveListeners.push(innerBlock);

				return false;
			} else {
				return true;
			}
		};

		this.initBlock = function ()
		{
			// inspect block children to identify nested blocks

			new DocumentScanner(this, true).scan(this.htmlElement);
		};

		this.top = function ()
		{
			return this.currentTop;
		};

		this.left = function ()
		{
			return this.currentLeft;
		};

		this.width = function ()
		{
			return this.htmlElement.offsetWidth;
		};

		this.height = function ()
		{
			return this.htmlElement.offsetHeight;
		};

		/*
		 * methods
		 */
		this.print = function ()
		{
			var output = 'block: ' + this.id;
			if (this.blocks.length > 0)
			{
				output += '<ul>';
				var i;
				for (i = 0; i < this.blocks.length; i++)
				{
					output += '<li>';
					output += this.blocks[i].print();
					output += '</li>';
				}
				output += '</ul>';
			}
			return output;
		};

		/*
		 * This function searches for a nested block (or the block itself) with a given id
		 */
		this.findBlock = function (blockId)
		{
			if (this.id === blockId)
				return this;

			var result;
			var i;
			for (i = 0; i < this.blocks.length && !result; i++)
			{
				result = this.blocks[i].findBlock(blockId);
			}

			return result;
		};

		this.move = function (left, top)
		{
			this.htmlElement.style.left = left;
			this.htmlElement.style.top = top;

			this.onMove();
		};

		this.onMove = function ()
		{
			var i;
			this.currentLeft = calculateOffsetLeft(this.htmlElement) - this.canvas.offsetLeft + calculateSideBarOffset();
			this.currentTop = calculateOffsetTop(this.htmlElement) - this.canvas.offsetTop;
			// notify listeners
			for (i = 0; i < this.moveListeners.length; i++)
			{
				this.moveListeners[i].onMove();
			}
		};

		this.toString = function ()
		{
			return 'block: ' + this.id;
		};
	}

	/**
	 * This class represents a connector segment, it is drawn via a div element.
	 * A segment has a starting point defined by the properties startX and startY, a length,
	 * a thickness and an orientation.
	 * Allowed values for the orientation property are defined by the constants UP, LEFT, DOWN and RIGHT.
	 *
	 * @param {type} id
	 * @param {type} parentElement
	 * @returns {js-graph-itApp.Segment}
	 */
	function Segment(id, parentElement)
	{
		this.id = id;
		this.htmlElement = document.createElement('div');
		this.htmlElement.id = id;
		this.htmlElement.style.position = 'absolute';
		this.htmlElement.style.overflow = 'hidden';

		if (parentElement.childNodes.length === 1) {
			parentElement.insertBefore(this.htmlElement, parentElement.childNodes[0]);
		} else {
			parentElement.appendChild(this.htmlElement);
		}

		this.startX;
		this.startY;
		this.length;
		this.thickness;
		this.rel;
		this.data;
		this.strategy;
		this.uid;
		this.orientation;
		this.nextSegment;
		this.visible = true;

		/**
		 * draw the segment. This operation is cascaded to next segment if any.
		 */
		this.draw = function ()
		{
			// set properties to next segment
			if (this.nextSegment)
			{
				this.nextSegment.startX = this.getEndX();
				this.nextSegment.startY = this.getEndY();
			}

			if (this.visible) {
				this.htmlElement.style.display = 'block';
			} else {
				this.htmlElement.style.display = 'none';
			}

			switch (this.orientation)
			{
				case LEFT:
					this.htmlElement.style.left = (this.startX - this.length) + "px";
					this.htmlElement.style.top = this.startY + "px";
					this.htmlElement.style.width = this.length + "px";
					this.htmlElement.style.height = this.thickness + "px";
					break;
				case RIGHT:
					this.htmlElement.style.left = this.startX + "px";
					this.htmlElement.style.top = this.startY + "px";
					if (this.nextSegment)
						this.htmlElement.style.width = this.length + this.thickness + "px";
					else
						this.htmlElement.style.width = this.length + "px";
					this.htmlElement.style.height = this.thickness + "px";
					break;
				case UP:
					this.htmlElement.style.left = this.startX + "px";
					this.htmlElement.style.top = (this.startY - this.length) + "px";
					this.htmlElement.style.width = this.thickness + "px";
					this.htmlElement.style.height = this.length + "px";
					break;
				case DOWN:
					this.htmlElement.style.left = this.startX + "px";
					this.htmlElement.style.top = this.startY + "px";
					this.htmlElement.style.width = this.thickness + "px";
					if (this.nextSegment)
						this.htmlElement.style.height = this.length + this.thickness + "px";
					else
						this.htmlElement.style.height = this.length + "px";
					break;
			}

			this.htmlElement.setAttribute("orientation", this.orientation);
			this.htmlElement.setAttribute("rel", this.rel);
			this.htmlElement.setAttribute("data", this.data);
			this.htmlElement.setAttribute("uid", this.uid);
			this.htmlElement.setAttribute("strategy", this.strategy);

			if (this.nextSegment) {
				this.nextSegment.draw();
			}
		};

		/**
		 * Returns the "left" coordinate of the end point of this segment
		 */
		this.getEndX = function ()
		{
			switch (this.orientation)
			{
				case LEFT:
					return this.startX - this.length;
				case RIGHT:
					return this.startX + this.length;
				case DOWN:
					return this.startX;
				case UP:
					return this.startX;
			}
		};

		/**
		 * Returns the "top" coordinate of the end point of this segment
		 */
		this.getEndY = function ()
		{
			switch (this.orientation)
			{
				case LEFT:
					return this.startY;
				case RIGHT:
					return this.startY;
				case DOWN:
					return this.startY + this.length;
				case UP:
					return this.startY - this.length;
			}
		};

		/**
		 * Append another segment to the end point of this.
		 * If another segment is already appended to this, cascades the operation so
		 * the given next segment will be appended to the tail of the segments chain.
		 *
		 * @param {type} nextSegment
		 * @returns {undefined}
		 */
		this.append = function (nextSegment)
		{
			if (!nextSegment) {
				return;
			}

			if (!this.nextSegment)
			{
				this.nextSegment = nextSegment;
				this.nextSegment.startX = this.getEndX();
				this.nextSegment.startY = this.getEndY();
			} else {
				this.nextSegment.append(nextSegment);
			}
		};

		this.detach = function ()
		{
			var s = this.nextSegment;
			this.nextSegment = null;

			return s;
		};

		/**
		 * hides this segment and all the following
		 */
		this.cascadeHide = function ()
		{
			this.visible = false;
			if (this.nextSegment) {
				this.nextSegment.cascadeHide();
			}
		};
	}

	/**
	 * Connector class.
	 * The init function takes two Block objects as arguments representing 
	 * the source and destination of the connector
	 *
	 * @param {type} htmlElement
	 * @param {type} canvas
	 * @returns {js-graph-itApp.Connector}
	 */
	function Connector(htmlElement, canvas)
	{
		/**
		 * declaring html element
		 */
		this.htmlElement = htmlElement;

		/**
		 * the canvas this connector is in
		 */
		this.canvas = canvas;

		/**
		 * the source block
		 */
		this.source = null;

		/**
		 * the destination block
		 */
		this.destination = null;

		/**
		 * preferred orientation
		 */
		this.preferredSourceOrientation = AUTO;
		this.preferredDestinationOrientation = AUTO;

		/**
		 * css class to be applied to the connector's segments
		 */
		this.connectorClass;

		/**
		 * minimum length for a connector segment.
		 */
		this.minSegmentLength = 40;

		/**
		 * size of the connector, i.e.: thickness of the segments.
		 */
		this.size = parseInt(this.htmlElement.attributes.size.value);

		/**
		 * connector's color
		 */
		this.color = 'black';

		/**
		 * move listeners, they are notify when connector moves
		 */
		this.moveListeners = new Array();

		this.firstSegment;

		this.segmentsPool;

		this.segmentsNumber = 0;

		this.strategy;

		this.connectorHtmlElement;

		this.initConnector = function ()
		{
			this.connectorHtmlElement = document.createElement('div');
			this.connectorHtmlElement.className = 'connectors';
			this.canvas.htmlElement.appendChild(this.connectorHtmlElement);

			// detect the connector id
			if (this.htmlElement.id) {
				this.id = this.htmlElement.id;
			} else {
				this.id = this.htmlElement.className;
			}

			// split the class name to get the ids of the source and destination blocks
			var splitted = htmlElement.className.split(' ');
			if (splitted.length < 3)
			{
				alert('Unable to create connector \'' + id + '\', class is not in the correct format: connector <sourceBlockId>, <destBlockId>');
				return;
			}

			this.connectorClass = splitted[0] + ' ' + splitted[1] + ' ' + splitted[2];

			this.source = this.canvas.findBlock(splitted[1]);
			if (!this.source)
			{
				alert('cannot find source block with id \'' + splitted[1] + '\'');
				return;
			}

			this.destination = this.canvas.findBlock(splitted[2]);
			if (!this.destination)
			{
				alert('cannot find destination block with id \'' + splitted[2] + '\'');
				return;
			}

			// check preferred orientation
			if (hasClass(this.htmlElement, 'vertical'))
			{
				this.preferredSourceOrientation = VERTICAL;
				this.preferredDestinationOrientation = VERTICAL;
			} else if (hasClass(this.htmlElement, 'horizontal'))
			{
				this.preferredSourceOrientation = HORIZONTAL;
				this.preferredDestinationOrientation = HORIZONTAL;
			} else
			{
				// check preferred orientation on source side
				if (hasClass(this.htmlElement, 'vertical_start')) {
					this.preferredSourceOrientation = VERTICAL;
				} else if (hasClass(this.htmlElement, 'horizontal_start')) {
					this.preferredSourceOrientation = HORIZONTAL;
				} else if (hasClass(this.htmlElement, 'left_start')) {
					this.preferredSourceOrientation = LEFT;
				} else if (hasClass(this.htmlElement, 'right_start')) {
					this.preferredSourceOrientation = RIGHT;
				} else if (hasClass(this.htmlElement, 'up_start')) {
					this.preferredSourceOrientation = UP;
				} else if (hasClass(this.htmlElement, 'down_start')) {
					this.preferredSourceOrientation = DOWN;
				}

				// check preferred orientation on destination side
				if (hasClass(this.htmlElement, 'vertical_end')) {
					this.preferredDestinationOrientation = VERTICAL;
				} else if (hasClass(this.htmlElement, 'horizontal_end')) {
					this.preferredDestinationOrientation = HORIZONTAL;
				} else if (hasClass(this.htmlElement, 'left_end')) {
					this.preferredDestinationOrientation = LEFT;
				} else if (hasClass(this.htmlElement, 'right_end')) {
					this.preferredDestinationOrientation = RIGHT;
				} else if (hasClass(this.htmlElement, 'up_end')) {
					this.preferredDestinationOrientation = UP;
				} else if (hasClass(this.htmlElement, 'down_end')) {
					this.preferredDestinationOrientation = DOWN;
				}
			}

			// get the first strategy as default
			this.strategy = strategies[0](this);
			this.repaint();

			this.source.moveListeners.push(this);
			this.destination.moveListeners.push(this);

			// call inspectors for this connector
			var i;
			for (i = 0; i < inspectors.length; i++)
			{
				inspectors[i].inspect(this);
			}

			// remove old html element
			this.htmlElement.parentNode.removeChild(this.htmlElement);
		};

		this.getStartSegment = function ()
		{
			return this.firstSegment;
		};

		this.getEndSegment = function ()
		{
			var s = this.firstSegment;
			while (s.nextSegment) {
				s = s.nextSegment;
			}

			return s;
		};

		this.getMiddleSegment = function ()
		{
			if (!this.strategy) {
				return null;
			}

			return this.strategy.getMiddleSegment();
		};

		this.getStrategy = function ()
		{
			if (!this.strategy) {
				return null;
			}

			return this.strategy.getStrategy();
		};

		this.createSegment = function ()
		{
			var segment;

			// if the pool contains more objects, borrow the segment, create it otherwise
			if (this.segmentsPool)
			{
				segment = this.segmentsPool;
				this.segmentsPool = this.segmentsPool.detach();
			} else
			{
				segment = new Segment(this.id + "_" + (this.segmentsNumber + 1), this.connectorHtmlElement);
				segment.htmlElement.className = this.connectorClass;
				if (!getStyle(segment.htmlElement, 'background-color'))
					segment.htmlElement.style.backgroundColor = this.color;
				segment.thickness = this.size;
				segment.rel = this.htmlElement.attributes.rel.value;
				segment.uid = this.htmlElement.attributes.uid.value;
				segment.data = this.htmlElement.attributes.data.value;
				segment.strategy = this.getStrategy();
			}
			this.segmentsNumber++;

			if (this.firstSegment) {
				this.firstSegment.append(segment);
			} else {
				this.firstSegment = segment;
			}
			segment.visible = true;

			return segment;
		};

		/**
		 * Repaints the connector
		 */
		this.repaint = function ()
		{
			// check strategies fitness and choose the best fitting one
			var i;
			var maxFitness = 0;
			var fitness;
			var s;

			// check if any strategy is possible with preferredOrientation
			for (i = 0; i < strategies.length; i++)
			{
				this.clearSegments();

				fitness = 0;
				s = strategies[i](this);
				if (s.isApplicable())
				{
					fitness++;
					s.paint();
					// check resulting orientation against the preferred orientations
					if ((this.firstSegment.orientation & this.preferredSourceOrientation) !== 0) {
						fitness++;
					}
					if ((this.getEndSegment().orientation & this.preferredDestinationOrientation) !== 0) {
						fitness++;
					}
				}

				if (fitness > maxFitness)
				{
					this.strategy = s;
					maxFitness = fitness;
				}
			}

			this.clearSegments();

			this.strategy.paint();
			this.firstSegment.draw();

			// this is needed to actually hide unused html elements	
			if (this.segmentsPool) {
				this.segmentsPool.draw();
			}
		};

		/**
		 * Hide all the segments and return them to pool
		 */
		this.clearSegments = function ()
		{
			if (this.firstSegment)
			{
				this.firstSegment.cascadeHide();
				this.firstSegment.append(this.segmentsPool);
				this.segmentsPool = this.firstSegment;
				this.firstSegment = null;
			}
		};

		this.onMove = function ()
		{
			this.repaint();

			// notify listeners
			var i;
			for (i = 0; i < this.moveListeners.length; i++) {
				this.moveListeners[i].onMove();
			}
		};
	}

	function ConnectorEnd(htmlElement, connector, side)
	{
		this.side = side;
		this.htmlElement = htmlElement;
		this.connector = connector;
		connector.canvas.htmlElement.appendChild(htmlElement);
		// strip extension
		if (this.htmlElement.tagName.toLowerCase() === "img")
		{
			this.src = this.htmlElement.src.substring(0, this.htmlElement.src.lastIndexOf('.'));
			this.srcExtension = this.htmlElement.src.substring(this.htmlElement.src.lastIndexOf('.'));
			this.htmlElement.style.zIndex = getStyle(this.connector.htmlElement, "z-index");
		}

		this.orientation;

		this.repaint = function ()
		{
			this.htmlElement.style.position = 'absolute';

			var left;
			var top;
			var segment;
			var orientation;

			if (this.side === START)
			{
				segment = connector.getStartSegment();
				left = segment.startX;
				top = segment.startY;
				orientation = segment.orientation;
				// swap orientation
				if ((orientation & VERTICAL) !== 0) {
					orientation = (~orientation) & VERTICAL;
				} else {
					orientation = (~orientation) & HORIZONTAL;
				}
			} else {
				segment = connector.getEndSegment();
				left = segment.getEndX();
				top = segment.getEndY();
				orientation = segment.orientation;
			}

			switch (orientation)
			{
				case LEFT:
					top -= (this.htmlElement.offsetHeight - segment.thickness) / 2;
					break;
				case RIGHT:
					left -= this.htmlElement.offsetWidth;
					top -= (this.htmlElement.offsetHeight - segment.thickness) / 2;
					break;
				case DOWN:
					top -= this.htmlElement.offsetHeight;
					left -= (this.htmlElement.offsetWidth - segment.thickness) / 2;
					break;
				case UP:
					left -= (this.htmlElement.offsetWidth - segment.thickness) / 2;
					break;
			}

			this.htmlElement.style.left = Math.ceil(left) + "px";
			this.htmlElement.style.top = Math.ceil(top) + "px";

			if (this.htmlElement.tagName.toLowerCase() === "img" && this.orientation !== orientation)
			{
				var orientationSuffix;
				switch (orientation)
				{
					case UP:
						orientationSuffix = "u";
						break;
					case DOWN:
						orientationSuffix = "d";
						break;
					case LEFT:
						orientationSuffix = "l";
						break;
					case RIGHT:
						orientationSuffix = "r";
						break;
				}
				this.htmlElement.src = this.src + "_" + orientationSuffix + this.srcExtension;
			}
			this.orientation = orientation;
		};

		this.onMove = function ()
		{
			this.repaint();
		};
	}

	function SideConnectorLabel(connector, htmlElement, side)
	{
		this.connector = connector;
		this.htmlElement = htmlElement;
		this.side = side;
		this.connector.htmlElement.parentNode.appendChild(htmlElement);

		this.repaint = function ()
		{
			this.htmlElement.style.position = 'absolute';
			var left;
			var top;
			var segment;

			if (this.side === START)
			{
				segment = this.connector.getStartSegment();
				left = segment.startX;
				top = segment.startY;
				if (segment.orientation === LEFT) {
					left -= this.htmlElement.offsetWidth;
				}
				if (segment.orientation === UP) {
					top -= this.htmlElement.offsetHeight;
				}
				if ((segment.orientation & HORIZONTAL) !== 0 && top < this.connector.getEndSegment().getEndY()) {
					top -= this.htmlElement.offsetHeight;
				}
				if ((segment.orientation & VERTICAL) !== 0 && left < this.connector.getEndSegment().getEndX()) {
					left -= this.htmlElement.offsetWidth;
				}
			} else
			{
				segment = this.connector.getEndSegment();
				left = segment.getEndX();
				top = segment.getEndY();
				if (segment.orientation === RIGHT) {
					left -= this.htmlElement.offsetWidth;
				}
				if (segment.orientation === DOWN) {
					top -= this.htmlElement.offsetHeight;
				}
				if ((segment.orientation & HORIZONTAL) !== 0 && top < this.connector.getStartSegment().startY) {
					top -= this.htmlElement.offsetHeight;
				}
				if ((segment.orientation & VERTICAL) !== 0 && left < this.connector.getStartSegment().startX) {
					left -= this.htmlElement.offsetWidth;
				}
			}

			this.htmlElement.style.left = Math.ceil(left) + "px";
			this.htmlElement.style.top = Math.ceil(top) + "px";
		};

		this.onMove = function ()
		{
			this.repaint();
		};
	}

	function MiddleConnectorLabel(connector, htmlElement)
	{
		this.connector = connector;
		this.htmlElement = htmlElement;
		this.connector.canvas.htmlElement.appendChild(htmlElement);

		this.repaint = function ()
		{
			this.htmlElement.style.position = 'absolute';

			var left;
			var top;
			var segment = connector.getMiddleSegment();

			if ((segment.orientation & VERTICAL) !== 0)
			{
				// put label at middle height on right side of the connector
				top = segment.htmlElement.offsetTop + (segment.htmlElement.offsetHeight - this.htmlElement.offsetHeight) / 2;
				left = segment.htmlElement.offsetLeft;
			} else {
				// put connector below the connector at middle widths
				top = segment.htmlElement.offsetTop;
				left = segment.htmlElement.offsetLeft + (segment.htmlElement.offsetWidth - this.htmlElement.offsetWidth) / 2;
				;
			}

			this.htmlElement.style.left = Math.ceil(left) + "px";
			this.htmlElement.style.top = Math.ceil(top) + "px";
			this.htmlElement.setAttribute('strategy', connector.getStrategy());
		};

		this.onMove = function ()
		{
			this.repaint();
		};
	}

	/*
	 * Inspector classes
	 */

	function ConnectorEndsInspector()
	{
		this.inspect = function (connector)
		{
			var children = connector.htmlElement.childNodes;
			var i;
			for (i = 0; i < children.length; i++)
			{
				if (hasClass(children[i], "connector-end"))
				{
					var newElement = new ConnectorEnd(children[i], connector, END);
					newElement.repaint();
					connector.moveListeners.push(newElement);
				} else if (hasClass(children[i], "connector-start"))
				{
					var newElement = new ConnectorEnd(children[i], connector, START);
					newElement.repaint();
					connector.moveListeners.push(newElement);
				}
			}
		};
	}

	function ConnectorLabelsInspector()
	{
		this.inspect = function (connector)
		{
			var children = connector.htmlElement.childNodes;
			var i;
			for (i = 0; i < children.length; i++)
			{
				if (hasClass(children[i], "source-label"))
				{
					var newElement = new SideConnectorLabel(connector, children[i], START);
					newElement.repaint();
					connector.moveListeners.push(newElement);
				} else if (hasClass(children[i], "middle-label"))
				{
					var newElement = new MiddleConnectorLabel(connector, children[i]);
					newElement.repaint();
					connector.moveListeners.push(newElement);
				} else if (hasClass(children[i], "destination-label"))
				{
					var newElement = new SideConnectorLabel(connector, children[i], END);
					newElement.repaint();
					connector.moveListeners.push(newElement);
				}
			}
		};
	}

	/*
	 * This function initializes the js_graph objects inspecting the html document
	 */
	function initPageObjects()
	{
		var divs = document.getElementsByTagName('div');
		var i;
		for (i = 0; i < divs.length; i++)
		{
			if (isCanvas(divs[i]) && !findCanvas(divs[i].id))
			{
				var newCanvas = new Canvas(divs[i]);
				newCanvas.reset();
				//newCanvas.initCanvas();
				canvases.push(newCanvas);
			}
		}
	}


	/*
	 * Utility functions
	 */


	function findCanvas(canvasId)
	{
		var i;
		for (i = 0; i < canvases.length; i++) {
			if (canvases[i].id === canvasId) {
				return canvases[i];
			}
		}

		return null;
	}

	function findBlock(blockId)
	{
		var i;
		for (i = 0; i < canvases.length; i++)
		{
			var block = canvases[i].findBlock(blockId);
			if (block) {
				return block;
			}
		}

		return null;
	}

	/*
	 * This function determines whether a html element is to be considered a canvas
	 */
	function isBlock(htmlElement)
	{
		return hasClass(htmlElement, 'block');
	}

	/*
	 * This function determines whether a html element is to be considered a block
	 */
	function isCanvas(htmlElement)
	{
		return hasClass(htmlElement, 'canvas');
	}

	/*
	 * This function determines whether a html element is to be considered a connector
	 */
	function isConnector(htmlElement)
	{
		return htmlElement.className && htmlElement.className.match(new RegExp('connector .*'));
	}

	/*
	 * This function calculates the absolute 'top' value for a html node
	 */
	function calculateOffsetTop(obj)
	{
		var curtop = 0;
		if (obj.offsetParent)
		{
			curtop = obj.offsetTop;
			while (obj = obj.offsetParent)
			{
				curtop += obj.offsetTop;
			}
		} else if (obj.y) {
			curtop += obj.y;
		}

		return curtop;
	}

	/*
	 * This function calculates the absolute 'left' value for a html node
	 */
	function calculateOffsetLeft(obj)
	{
		var curleft = 0;
		if (obj.offsetParent)
		{
			curleft = obj.offsetLeft;
			while (obj = obj.offsetParent)
			{
				curleft += obj.offsetLeft;
			}
		} else if (obj.x) {
			curleft += obj.x;
		}

		return curleft;
	}

	function parseBorder(obj, side)
	{
		var sizeString = getStyle(obj, "border-" + side + "-width");
		if (sizeString && sizeString !== "")
		{
			if (sizeString.substring(sizeString.length - 2) === "px") {
				return parseInt(sizeString.substring(0, sizeString.length - 2));
			}
		}

		return 0;
	}

	function hasClass(element, className)
	{
		if (!element || !element.className) {
			return false;
		}

		var classes = element.className.split(' ');
		var i;
		for (i = 0; i < classes.length; i++) {
			if (classes[i] === className) {
				return true;
			}
		}

		return false;
	}

	/**
	 * This function retrieves the actual value of a style property even if it is set via css.
	 *
	 * @param {type} node
	 * @param {type} styleProp
	 * @returns {node.currentStyle|undefined}
	 */
	function getStyle(node, styleProp)
	{
		// if not an element
		if (node.nodeType !== 1) {
			return;
		}

		var value;
		if (node.currentStyle) {
			// ie case
			styleProp = replaceDashWithCamelNotation(styleProp);
			value = node.currentStyle[styleProp];
		} else if (window.getComputedStyle) {
			// mozilla case
			value = document.defaultView.getComputedStyle(node, null).getPropertyValue(styleProp);
		}

		return value;
	}

	function replaceDashWithCamelNotation(value)
	{
		var pos = value.indexOf('-');
		while (pos > 0 && value.length > pos + 1)
		{
			value = value.substring(0, pos) + value.substring(pos + 1, pos + 2).toUpperCase() + value.substring(pos + 2);
			pos = value.indexOf('-');
		}

		return value;
	}


	/*******************************
	 * Connector paint strategies. *
	 *******************************/

	/**
	 * Horizontal "S" routing strategy.
	 *
	 * @param {type} connector
	 * @returns {js-graph-itApp.HorizontalSStrategy}
	 */
	function HorizontalSStrategy(connector)
	{
		this.connector = connector;

		this.startSegment;
		this.middleSegment;
		this.endSegment;

		this.strategyName = "horizontal_s";

		this.getStrategy = function ()
		{
			return this.strategyName;
		};

		this.getMiddleSegment = function ()
		{
			return this.middleSegment;
		};

		this.isApplicable = function ()
		{
			var sourceLeft = this.connector.source.left();
			var sourceWidth = this.connector.source.width();
			var destinationLeft = this.connector.destination.left();
			var destinationWidth = this.connector.destination.width();

			return Math.abs(2 * destinationLeft + destinationWidth - (2 * sourceLeft + sourceWidth)) - (sourceWidth + destinationWidth) > 4 * this.connector.minSegmentLength;
		};

		this.paint = function ()
		{
			this.startSegment = connector.createSegment();
			this.middleSegment = connector.createSegment();
			this.endSegment = connector.createSegment();

			var sourceLeft = this.connector.source.left();
			var sourceTop = this.connector.source.top();
			var sourceWidth = this.connector.source.width();
			var sourceHeight = this.connector.source.height();

			var destinationLeft = this.connector.destination.left();
			var destinationTop = this.connector.destination.top();
			var destinationWidth = this.connector.destination.width();
			var destinationHeight = this.connector.destination.height();

			var hLength;

			this.startSegment.startY = Math.floor(sourceTop + sourceHeight / 2) - (this.connector.size / 2);

			// deduce which face to use on source and destination blocks
			if (sourceLeft + sourceWidth / 2 < destinationLeft + destinationWidth / 2)
			{
				// use left side of the source block and right side of the destination block
				this.startSegment.startX = sourceLeft + sourceWidth;
				hLength = destinationLeft - (sourceLeft + sourceWidth);
			} else
			{
				// use right side of the source block and left side of the destination block
				this.startSegment.startX = sourceLeft;
				hLength = destinationLeft + destinationWidth - sourceLeft;
			}

			// first horizontal segment positioning
			this.startSegment.length = Math.floor(Math.abs(hLength) / 2);
			this.startSegment.orientation = hLength > 0 ? RIGHT : LEFT;

			// vertical segment positioning			
			var vLength = Math.floor(destinationTop + destinationHeight / 2 - (sourceTop + sourceHeight / 2));
			this.middleSegment.length = Math.abs(vLength);
			if (vLength === 0) {
				this.middleSegment.visible = false;
			}
			this.middleSegment.orientation = vLength > 0 ? DOWN : UP;

			// second horizontal segment positioning
			this.endSegment.length = Math.floor(Math.abs(hLength) / 2);
			this.endSegment.orientation = hLength > 0 ? RIGHT : LEFT;
		};
	}

	/**
	 * Vertical "S" routing strategy.
	 *
	 * @param {type} connector
	 * @returns {js-graph-itApp.VerticalSStrategy}
	 */
	function VerticalSStrategy(connector)
	{
		this.connector = connector;

		this.startSegment;
		this.middleSegment;
		this.endSegment;

		this.strategyName = "vertical_s";

		this.getStrategy = function ()
		{
			return this.strategyName;
		};

		this.getMiddleSegment = function ()
		{
			return this.middleSegment;
		};

		this.isApplicable = function ()
		{
			var sourceTop = this.connector.source.top();
			var sourceHeight = this.connector.source.height();
			var destinationTop = this.connector.destination.top();
			var destinationHeight = this.connector.destination.height();

			return Math.abs(2 * destinationTop + destinationHeight - (2 * sourceTop + sourceHeight)) - (sourceHeight + destinationHeight) > 4 * this.connector.minSegmentLength;
		};

		this.paint = function ()
		{
			this.startSegment = connector.createSegment();
			this.middleSegment = connector.createSegment();
			this.endSegment = connector.createSegment();

			var sourceLeft = this.connector.source.left();
			var sourceTop = this.connector.source.top();
			var sourceWidth = this.connector.source.width();
			var sourceHeight = this.connector.source.height();

			var destinationLeft = this.connector.destination.left();
			var destinationTop = this.connector.destination.top();
			var destinationWidth = this.connector.destination.width();
			var destinationHeight = this.connector.destination.height();

			var vLength;

			this.startSegment.startX = Math.floor(sourceLeft + sourceWidth / 2) - (this.connector.size / 2);

			// deduce which face to use on source and destination blocks
			if (sourceTop + sourceHeight / 2 < destinationTop + destinationHeight / 2)
			{
				// use bottom side of the source block and top side of destination block
				this.startSegment.startY = sourceTop + sourceHeight;
				vLength = destinationTop - (sourceTop + sourceHeight);
			} else {
				// use top side of the source block and bottom side of the destination block
				this.startSegment.startY = sourceTop;
				vLength = destinationTop + destinationHeight - sourceTop;
			}

			// first vertical segment positioning
			this.startSegment.length = Math.floor(Math.abs(vLength) / 2);
			this.startSegment.orientation = vLength > 0 ? DOWN : UP;

			// horizontal segment positioning
			var hLength = Math.floor(destinationLeft + destinationWidth / 2 - (sourceLeft + sourceWidth / 2));
			this.middleSegment.orientation = hLength > 0 ? RIGHT : LEFT;
			this.middleSegment.length = Math.abs(hLength);

			// second vertical segment positioning
			this.endSegment.length = Math.floor(Math.abs(vLength) / 2);
			this.endSegment.orientation = vLength > 0 ? DOWN : UP;
		};
	}

	/**
	 * A horizontal "L" connector routing strategy
	 *
	 * @param {type} connector
	 * @returns {js-graph-itApp.HorizontalLStrategy}
	 */
	function HorizontalLStrategy(connector)
	{
		this.connector = connector;

		this.destination;

		this.startSegment;
		this.endSegment;

		this.strategyName = "horizontal_L";

		this.getStrategy = function ()
		{
			return this.strategyName;
		};

		this.isApplicable = function ()
		{
			var destMiddle = Math.floor(this.connector.destination.left() + this.connector.destination.width() / 2);
			var sl = this.connector.source.left();
			var sw = this.connector.source.width();
			var dt = this.connector.destination.top();
			var dh = this.connector.destination.height();
			var sourceMiddle = Math.floor(this.connector.source.top() + this.connector.source.height() / 2);

			if (destMiddle > sl && destMiddle < sl + sw) {
				return false;
			}
			if (sourceMiddle > dt && sourceMiddle < dt + dh) {
				return false;
			}

			return true;
		};

		/**
		 * Chooses the longest segment as the "middle" segment.
		 */
		this.getMiddleSegment = function ()
		{
			if (this.startSegment.length > this.endSegment.length) {
				return this.startSegment;
			}

			return this.endSegment;
		};

		this.paint = function ()
		{
			this.startSegment = this.connector.createSegment();
			this.endSegment = this.connector.createSegment();

			var destMiddleX = Math.floor(this.connector.destination.left() + this.connector.destination.width() / 2);
			var sl = this.connector.source.left();
			var sw = this.connector.source.width();
			var dt = this.connector.destination.top();
			var dh = this.connector.destination.height();

			this.startSegment.startY = Math.floor(this.connector.source.top() + this.connector.source.height() / 2) - (this.connector.size / 2);

			// decide which side of the source block to connect to
			if (Math.abs(destMiddleX - sl) < Math.abs(destMiddleX - (sl + sw)))
			{
				// use the left face
				this.startSegment.orientation = (destMiddleX < sl) ? LEFT : RIGHT;
				this.startSegment.startX = sl;
			} else {
				// use the right face
				this.startSegment.orientation = (destMiddleX > (sl + sw)) ? RIGHT : LEFT;
				this.startSegment.startX = sl + sw;
			}

			this.startSegment.length = Math.abs(destMiddleX - this.startSegment.startX - (this.connector.size / 2));

			// decide which side of the destination block to connect to
			if (Math.abs(this.startSegment.startY - dt) < Math.abs(this.startSegment.startY - (dt + dh)))
			{
				// use the upper face
				this.endSegment.orientation = (this.startSegment.startY < dt) ? DOWN : UP;
				this.endSegment.length = Math.abs(this.startSegment.startY - dt);
			} else {
				// use the lower face
				this.endSegment.orientation = (this.startSegment.startY > (dt + dh)) ? UP : DOWN;
				this.endSegment.length = Math.abs(this.startSegment.startY - (dt + dh));
			}
		};
	}

	/**
	 * Vertical "L" connector routing strategy
	 *
	 * @param {type} connector
	 * @returns {js-graph-itApp.VerticalLStrategy}
	 */
	function VerticalLStrategy(connector)
	{
		this.connector = connector;

		this.startSegment;
		this.endSegment;

		this.strategyName = "vertical_L";

		this.getStrategy = function ()
		{
			return this.strategyName;
		};

		this.isApplicable = function ()
		{
			var sourceMiddle = Math.floor(this.connector.source.left() + this.connector.source.width() / 2);
			var dl = this.connector.destination.left();
			var dw = this.connector.destination.width();
			var st = this.connector.source.top();
			var sh = this.connector.source.height();
			var destMiddle = Math.floor(this.connector.destination.top() + this.connector.destination.height() / 2);

			if (sourceMiddle > dl && sourceMiddle < dl + dw) {
				return false;
			}
			if (destMiddle > st && destMiddle < st + sh) {
				return false;
			}

			return true;
		};

		/**
		 * Chooses the longest segment as the "middle" segment.
		 */
		this.getMiddleSegment = function ()
		{
			if (this.startSegment.length > this.endSegment.length) {
				return this.startSegment;
			}

			return this.endSegment;
		};

		this.paint = function ()
		{
			this.startSegment = this.connector.createSegment();
			this.endSegment = this.connector.createSegment();

			var destMiddleY = Math.floor(this.connector.destination.top() + this.connector.destination.height() / 2) - this.connector.size;
			var dl = this.connector.destination.left();
			var dw = this.connector.destination.width();
			var st = this.connector.source.top();
			var sh = this.connector.source.height();

			this.startSegment.startX = Math.floor(this.connector.source.left() + this.connector.source.width() / 2) - (this.connector.size / 2);

			// decide which side of the source block to connect to
			if (Math.abs(destMiddleY - st) < Math.abs(destMiddleY - (st + sh)))
			{
				// use the upper face
				this.startSegment.orientation = (destMiddleY < st) ? UP : DOWN;
				this.startSegment.startY = st;
			} else {
				// use the lower face
				this.startSegment.orientation = (destMiddleY > (st + sh)) ? DOWN : UP;
				this.startSegment.startY = st + sh;
			}

			this.startSegment.length = Math.abs(destMiddleY - this.startSegment.startY) - (this.connector.size / 2);

			// decide which side of the destination block to connect to
			if (Math.abs(this.startSegment.startX - dl) < Math.abs(this.startSegment.startX - (dl + dw)))
			{
				// use the left face
				this.endSegment.orientation = (this.startSegment.startX < dl) ? RIGHT : LEFT;
				this.endSegment.length = Math.abs(this.startSegment.startX - dl);
			} else {
				// use the right face
				this.endSegment.orientation = (this.startSegment.startX > dl + dw) ? LEFT : RIGHT;
				this.endSegment.length = Math.abs(this.startSegment.startX - (dl + dw));
			}
		};
	}

	function HorizontalCStrategy(connector, startOrientation)
	{
		this.connector = connector;

		this.startSegment;
		this.middleSegment;
		this.endSegment;

		this.strategyName = "horizontal_c";

		this.getStrategy = function ()
		{
			return this.strategyName + '_' + startOrientation;
		};

		this.getMiddleSegment = function ()
		{
			return this.middleSegment;
		};

		this.isApplicable = function ()
		{
			return true;
		};

		this.paint = function ()
		{
			this.startSegment = connector.createSegment();
			this.middleSegment = connector.createSegment();
			this.endSegment = connector.createSegment();

			var sign = 1;
			if (startOrientation === RIGHT) {
				sign = -1;
			}

			var startX = this.connector.source.left();
			if (startOrientation === RIGHT) {
				startX += this.connector.source.width();
			}
			var startY = Math.floor(this.connector.source.top() + this.connector.source.height() / 2) - (this.connector.size / 2);

			var endX = this.connector.destination.left();
			if (startOrientation === RIGHT) {
				endX += this.connector.destination.width();
			}
			var endY = Math.floor(this.connector.destination.top() + this.connector.destination.height() / 2) - (this.connector.size / 2);

			this.startSegment.startX = startX;
			this.startSegment.startY = startY;
			this.startSegment.orientation = startOrientation;
			this.startSegment.length = this.connector.minSegmentLength + Math.max(0, sign * (startX - endX));

			var vLength = endY - startY;
			this.middleSegment.orientation = vLength > 0 ? DOWN : UP;
			this.middleSegment.length = Math.abs(vLength);

			this.endSegment.orientation = startOrientation === LEFT ? RIGHT : LEFT;
			this.endSegment.length = Math.max(0, sign * (endX - startX)) + this.connector.minSegmentLength;
		};
	}

	function VerticalCStrategy(connector, startOrientation)
	{
		this.connector = connector;

		this.startSegment;
		this.middleSegment;
		this.endSegment;

		this.strategyName = "vertical_c";

		this.getStrategy = function ()
		{
			return this.strategyName + '_' + startOrientation;
		};

		this.getMiddleSegment = function ()
		{
			return this.middleSegment;
		};

		this.isApplicable = function ()
		{
			return true;
		};

		this.paint = function ()
		{
			this.startSegment = connector.createSegment();
			this.middleSegment = connector.createSegment();
			this.endSegment = connector.createSegment();

			var sign = 1;
			if (startOrientation === DOWN)
				sign = -1;

			var startY = this.connector.source.top();
			if (startOrientation === DOWN) {
				startY += this.connector.source.height();
			}
			var startX = Math.floor(this.connector.source.left() + this.connector.source.width() / 2) - (this.connector.size / 2);

			var endY = this.connector.destination.top();
			if (startOrientation === DOWN) {
				endY += this.connector.destination.height();
			}
			var endX = Math.floor(this.connector.destination.left() + this.connector.destination.width() / 2) - (this.connector.size / 2);

			this.startSegment.startX = startX;
			this.startSegment.startY = startY;
			this.startSegment.orientation = startOrientation;
			this.startSegment.length = this.connector.minSegmentLength + Math.max(0, sign * (startY - endY));

			var hLength = endX - startX;
			this.middleSegment.orientation = hLength > 0 ? RIGHT : LEFT;
			this.middleSegment.length = Math.abs(hLength);

			this.endSegment.orientation = startOrientation === UP ? DOWN : UP;
			this.endSegment.length = Math.max(0, sign * (endY - startY)) + this.connector.minSegmentLength;
		};
	}

	function toggleFullscreen(elem) {
		elem = elem || document.documentElement;
		if (!document.fullscreenElement && !document.mozFullScreenElement &&
				!document.webkitFullscreenElement && !document.msFullscreenElement) {
			if (elem.requestFullscreen) {
				elem.requestFullscreen();
			} else if (elem.msRequestFullscreen) {
				elem.msRequestFullscreen();
			} else if (elem.mozRequestFullScreen) {
				elem.mozRequestFullScreen();
			} else if (elem.webkitRequestFullscreen) {
				elem.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
			}
		} else {
			if (document.exitFullscreen) {
				document.exitFullscreen();
			} else if (document.msExitFullscreen) {
				document.msExitFullscreen();
			} else if (document.mozCancelFullScreen) {
				document.mozCancelFullScreen();
			} else if (document.webkitExitFullscreen) {
				document.webkitExitFullscreen();
			}
		}
	}

	function parseTime(time) {
		var dateParts = time.split(" ")[0].split("/");
		var timeParts = time.split(" ")[1].split(":");

		var date = new Date(dateParts[0], (dateParts[1] - 1), dateParts[2], timeParts[0], timeParts[1], timeParts[2]);

		return date;
	}

	var timer;

	function updateTime(data) {
		var timeReal = document.getElementById('timeReal');
		var timeCurrent = document.getElementById('timeCurrent');

		if (timeReal && timeCurrent) {
			timeReal.innerHTML = data.time_real;
			timeCurrent.innerHTML = data.time_current;

			realTime = parseTime(data.time_real);
			var refresh = parseInt(appConfig.time_refresh);

			if (currentTime < realTime) {
				currentTime = parseTime(data.time_current);
				currentTime.setSeconds(currentTime.getSeconds() + refresh);
			} else {
				var toolbar = document.getElementById('timelineToolbar');
				var diff = Math.round((currentTime.getTime() - realTime.getTime()) / 1000);

				slider.set(diff);
				toolbar.innerHTML = diff + ' s<br />';
				toolbar.innerHTML += formatDate(parseTime(data.time_current));
			}
		}
	}

	function formatDate(dateObject) {
		return (dateObject.getHours() < 10 ? '0' : '') + dateObject.getHours() + ":" +
				(dateObject.getMinutes() < 10 ? '0' : '') + dateObject.getMinutes() + ":" +
				(dateObject.getSeconds() < 10 ? '0' : '') + dateObject.getSeconds();
	}

	function isNumeric(n) {
		return !isNaN(parseFloat(n)) && isFinite(n);
	}

	function formatNumber(number) {
		return isNumeric(number) ? Math.round(number * 100) / 100 : number;
	}

	var modalElementOriginal;
	var modalHtmlOriginal = '';

	function showModal(id) {
		hideModal();

		querySelector('.modal-wrapper').classList.remove("hidden");
		querySelector('.modal-wrapper').classList.add(id);

		var modalContainer = document.getElementById(id);
		modalContainer.classList.remove("hidden");
		modalHtmlOriginal = modalContainer.innerHTML;
		modalElementOriginal = modalContainer;
	}

	function hideModal() {
		querySelector('.modal-wrapper').classList.add("hidden");

		var modalContainerInfo = document.getElementById('modalContainerInfo');
		modalContainerInfo.classList.add("hidden");

		var modalContainerObject = document.getElementById('modalContainerObject');
		modalContainerObject.classList.add("hidden");

		var modalContainerComponent = document.getElementById('modalContainerComponent');
		modalContainerComponent.classList.add("hidden");

		var modalContainerConnector = document.getElementById('modalContainerConnector');
		modalContainerConnector.classList.add("hidden");

		if (modalElementOriginal) {
			while (modalElementOriginal.firstChild) {
				modalElementOriginal.removeChild(modalElementOriginal.firstChild);
			}
			modalElementOriginal.innerHTML = modalHtmlOriginal;
		}
	}

	var initTimeLine = function () {
		var keyboardSlider = document.getElementById('timeline');
		var toolbar = document.getElementById('timelineToolbar');

		if (!keyboardSlider) {
			return;
		}

		slider = noUiSlider.create(keyboardSlider, {
			start: 0,
			step: parseInt(appConfig.time_refresh),
			range: {
				'min': -parseInt(appConfig.time_range),
				'max': parseInt(appConfig.time_range)
			}
		});

		slider.on('change.one', function (value) {
			time = toolbar.innerHTML;
			refreshCanvases();
		});

		slider.on('update.one', function (value) {
			if (realTime) {
				currentTime = new Date(realTime.getTime());
				currentTime.setSeconds(currentTime.getSeconds() + Math.round(value[0]));

				toolbar.innerHTML = Math.round(value[0]) + ' s<br />';
				toolbar.innerHTML += formatDate(currentTime);
			}
		});

		var handle = keyboardSlider.querySelector('.noUi-handle');
		handle.innerHTML = toolbar.outerHTML;
		toolbar.parentNode.removeChild(toolbar);
		toolbar = document.getElementById('timelineToolbar');

		handle.addEventListener('keydown', function (e) {
			var value = Number(keyboardSlider.noUiSlider.get());

			if (e.which === 37) {
				keyboardSlider.noUiSlider.set(value - 10);
			}

			if (e.which === 39) {
				keyboardSlider.noUiSlider.set(value + 10);
			}
		});
	};

	var appendHtml = function (el, str) {
		var div = document.createElement('div');
		div.innerHTML = str;

		while (div.children.length > 0) {
			el.appendChild(div.children[0]);
		}
	};

	var closeAllMenu = function () {
		for (var j = 0; j < subnavs.length; j++) {
			subnavs[j].classList.remove('menu--subitens__opened');
		}
	};

	var initSideBar = function () {
		querySelector('.collapse_menu').onclick = function () {
			nav.classList.toggle('vertical_nav__minify');
			wrapper.classList.toggle('wrapper__minify');

			for (var j = 0; j < subnavs.length; j++) {
				subnavs[j].classList.remove('menu--subitens__opened');
			}
		};

		// Open Sub Menu
		for (var i = 0; i < subnavs.length; i++) {
			if (subnavs[i].classList.contains('menu--item__has_sub_menu')) {
				subnavs[i].querySelector('.menu--link').addEventListener('click', function (e) {

					for (var j = 0; j < subnavs.length; j++) {
						if (e.target.offsetParent !== subnavs[j]) {
							subnavs[j].classList.remove('menu--subitens__opened');
						}
					}

					e.target.offsetParent.classList.toggle('menu--subitens__opened');
				}, false);
			}
		}

		fireEvent(querySelector('.collapse_menu'), 'click');
	};

	var initImages = function () {
		var menuImages = document.querySelectorAll('.menu-object-wrapper > img');

		menuImages.forEach(function (image) {
			if (images[image.alt]) {
				image.src = images[image.alt].src;

			} else {
				console.log(images, image.alt)
			}
		});
	};

	var clearPageObjects = function () {
		for (var i in canvases) {
			while (canvases[i].htmlElement.firstChild) {
				canvases[i].htmlElement.removeChild(canvases[i].htmlElement.firstChild);
			}

			canvases[i].reset();
		}

		canvases = new Array();
	};

	var refreshCanvases = function () {
		initRequests(true);
	};

	var menuClick = function (index, param1, param2, param3) {
		console.log(index);

		closeAllMenu();

		switch (index) {
			case 'object-add':
				showModal('modalContainerObject');
				handleCheckBoxes();
				break;
			case 'refresh':
				initRequests(true);
				break;
			case 'fullscreen':
				var viewFullScreen = document.getElementsByClassName("app-body");

				if (viewFullScreen[0]) {
					toggleFullscreen(viewFullScreen[0]);
				}
				break;
				/*case 'edit-mode':
				 for (var i in canvases) {
				 canvases[i].htmlElement.classList.toggle('edit');
				 }
				 
				 editMode = !editMode;
				 break;*/
			case 'entity-place':
				appAjaxPost(routeApi + 'entity', {
					uid: param1,
					layer: layer
				}, function () {
					refreshCanvases();
				});
				break;
			case 'connector-place':
				placingConnector = !placingConnector;

				if (!placingConnector) {
					connectorType = null;
					firstConnector = null;
					document.body.classList.remove("placing-connector");
				} else {
					connectorType = parseInt(param1);
					document.body.classList.add("placing-connector");
				}
				break;
		}
	};

	var initRequests = function (clearAll = false) {
		//console.log(currentTime)
		var parameters = appConfig.editor === true ? {} : {
			time: currentTime.getFullYear() + '/' +
					((currentTime.getMonth() + 1) < 10 ? '0' + (currentTime.getMonth() + 1) : (currentTime.getMonth() + 1)) + '/' +
					(currentTime.getDate() < 10 ? '0' + currentTime.getDate() : currentTime.getDate()) + ' ' +
					(currentTime.getHours() < 10 ? '0' : '') + currentTime.getHours() + ":" +
					(currentTime.getMinutes() < 10 ? '0' : '') + currentTime.getMinutes() + ":" +
					(currentTime.getSeconds() < 10 ? '0' : '') + currentTime.getSeconds()
		};

		appAjaxPost(routeApi + 'data', parameters, function (data) {
			if (clearAll) {
				clearPageObjects();
				initPageObjects();
			}

			if (data.debug) {
				console.log(data.debug);
			}

			var html = '', length;

			var directions = {
				'top': function (length) {
					return 'left: ' + length + '%; top: 0px;';
				},
				'bottom': function (length) {
					return 'left: ' + length + '%; bottom: 0px;';
				},
				'left': function (length) {
					return 'top: ' + length + '%; left: 0px;';
				},
				'right': function (length) {
					return 'top: ' + length + '%; right: 0px;';
				}
			};

			var getPipeSize = function (uid) {
				switch (uid) {
					case 'line':
						return 3;
					default:
						return 6;
				}
			};

			updateTime(data);

			var entities = {};
			var connectorMarkers = '<div dir="1" class="connector-marker connector-marker-left"></div>' +
					'<div dir="2" class="connector-marker connector-marker-right"></div>' +
					'<div dir="4" class="connector-marker connector-marker-top"></div>' +
					'<div dir="8" class="connector-marker connector-marker-bottom"></div>';

			var visibleMarkers = '<div class="marker-align marker-align-top"></div>' +
					'<div class="marker-align marker-align-left"></div>' +
					'<div class="marker-align marker-align-bottom"></div>' +
					'<div class="marker-align marker-align-right"></div>';

			for (var i in data.entities) {
				var entity = data.entities[i];

				html += '<div alt="' + entity.name + '" dataobject="' + encodeURIComponent(entity.data_object) + '" data="' + encodeURIComponent(entity.data) +
						'" id="entity_' + entity.entity_id + '" class="block draggable" style="left: ' + entity.pos_x + 'px; top: ' + entity.pos_y + 'px;">';
				html += connectorMarkers;
				html += visibleMarkers;
				html += '<img class="block-image" alt="' + entity.uid + '" width="' + entity.width + '" height="' + entity.height + '" src="' + images[entity.uid].src + '" />';

				if (data.io.hasOwnProperty(entity.entity_id)) {
					for (var direction in directions) {
						if (data.io[entity.entity_id].hasOwnProperty(direction)) {
							length = -(100 / data.io[entity.entity_id][direction]) / 2;

							for (var u = 1; u <= data.io[entity.entity_id][direction]; u++) {
								length += 100 / data.io[entity.entity_id][direction];
								html += '	<div id="entity_' + entity.entity_id + '_' + direction + '_' + u + '" class="block dock_point" style="' + directions[direction](length) + '"></div>';
							}
						}
					}
				}

				var dataEntity = JSON.parse(entity.data) || {};
				var dataObject = JSON.parse(entity.data_object) || {};

				dataEntity = extend({
					parameters: {},
					visible: []
				}, dataEntity);

				dataObject = extend({
					parameters: {},
					visible: [],
					required: []
				}, dataObject);

				var html2 = '';

				if (appConfig.editor === false) {
					if ((Object.keys(dataEntity).length && Object.keys(dataEntity.parameters).length) || Object.keys(dataObject.parameters).length) {
						html2 += '<div class="entity-table-data" style="left: ' + (entity.width) + 'px; ">' +
								'<table cellpadding="3" cellspacing="3" border="0" width="100%"><tbody>';

						var showObjectAttributes = false;
						var showEntityAttributes = false;

						for (var i in dataObject.parameters) {
							if (dataObject.visible.indexOf(i) !== -1) {
								html2 += '<tr>';
								html2 += '<td><label class="entity-table-data-label">' + i + ':</label></td>';
								html2 += '<td><label class="entity-table-data-value">' + formatNumber(dataObject.parameters[i]) + '</label></td>';
								html2 += '</tr>';
								showObjectAttributes = true;
							}
						}

						if (Object.keys(dataEntity.parameters).length) {

							for (var i in dataEntity.parameters) {
								if (dataEntity.visible.indexOf(i) !== -1) {
									if (showObjectAttributes) {
										html2 += '<tr>';
										html2 += '<td colspan="2"><div class="entity-table-data-line"></div><td>';
										html2 += '</tr>';
									}
									break;
								}
							}

							for (var i in dataEntity.parameters) {
								if (dataEntity.visible.indexOf(i) !== -1) {
									html2 += '<tr>';
									html2 += '<td><label class="entity-table-data-label">' + i + ':</label></td>';
									html2 += '<td><label class="entity-table-data-value">' + formatNumber(dataEntity.parameters[i]) + '</label></td>';
									html2 += '</tr>';
									showEntityAttributes = true;
								}
							}
						}

						html2 += '</tbody></table>' +
								'</div>';
					}
				}

				entities[entity.entity_id] = {
					top: 0,
					left: 0,
					right: 0,
					bottom: 0
				};

				if (showObjectAttributes || showEntityAttributes) {
					html += html2;
				}

				html += '</div>';
			}


			/*
			 var LEFT = 1;
			 var RIGHT = 2;
			 var UP = 4;
			 var DOWN = 8;
			 var HORIZONTAL = LEFT + RIGHT;
			 var VERTICAL = UP + DOWN;
			 var AUTO = HORIZONTAL + VERTICAL;
			 */

			var connectorData;

			for (var i in data.connectors) {
				var connector = data.connectors[i];
				var classStart = '', classEnd = '';
				var entityFrom = '', entityTo = '';

				var dataOriginal = {
					size: getPipeSize(connector.uid),
					parameters: null
				};

				connectorData = extend(dataOriginal, JSON.parse(connector.data) || {});

				if (entities[connector.entity_from]) {
					if (connector.dir_start === LEFT) {
						classStart = ' left_start';
						entityFrom = '_left_' + (++entities[connector.entity_from].left);
					} else if (connector.dir_start === RIGHT) {
						classStart = ' right_start';
						entityFrom = '_right_' + (++entities[connector.entity_from].right);
					} else if (connector.dir_start === UP) {
						classStart = ' up_start';
						entityFrom = '_top_' + (++entities[connector.entity_from].top);
					} else if (connector.dir_start === DOWN) {
						classStart = ' down_start';
						entityFrom = '_bottom_' + (++entities[connector.entity_from].bottom);
					} else if (connector.dir_start === HORIZONTAL) {
						classStart = ' horizontal_start';
					} else if (connector.dir_start === VERTICAL) {
						classStart = ' vertical_start';
					}
				}

				if (entities[connector.entity_to]) {
					if (connector.dir_end === LEFT) {
						classEnd = ' right_end';
						entityTo = '_left_' + (++entities[connector.entity_to].left);
					} else if (connector.dir_end === RIGHT) {
						classEnd = ' left_end';
						entityTo = '_right_' + (++entities[connector.entity_to].right);
					} else if (connector.dir_end === UP) {
						classEnd = ' down_end';
						entityTo = '_top_' + (++entities[connector.entity_to].top);
					} else if (connector.dir_end === DOWN) {
						classEnd = ' up_end';
						entityTo = '_bottom_' + (++entities[connector.entity_to].bottom);
					} else if (connector.dir_end === HORIZONTAL) {
						classEnd = ' horizontal_end';
					} else if (connector.dir_end === VERTICAL) {
						classEnd = ' vertical_end';
					}
				}

				html += '<div size="' + connectorData.size + '" data="' + encodeURIComponent(connector.data) + '" uid="' + connector.uid + '" rel="' + connector.id + '" class="connector entity_' + connector.entity_from +
						entityFrom + ' entity_' + connector.entity_to + entityTo +
						classStart + classEnd + '">';

				if (connector.uid === 'line') {
					html += '	<img class="connector-end" src="assets/img/arrow.png" />';
				}

				var labelContent = '';

				if (appConfig.editor === false) {
					var dataConnector = JSON.parse(connector.data) || {};

					if (Object.keys(dataConnector).length) {
						labelContent += '<div class="connector-table-data">' +
								'<table cellpadding="3" cellspacing="3" border="0" width="100%"><tbody>';

						for (var i in dataConnector) {
							labelContent += '<tr>';
							labelContent += '<td><label class="entity-table-data-label">' + i + ':</label></td>';
							labelContent += '<td><label class="entity-table-data-value">' + formatNumber(dataConnector[i]) + '</label></td>';
							labelContent += '</tr>';
						}

						labelContent += '</tbody></table>' +
								'</div>';
					}
				}

				if (labelContent !== '') {
					html += '	<label class="middle-label">' + labelContent + '</label>';
				}

				html += '</div>';
			}

			for (var i in canvases) {
				canvases[i].htmlElement.innerHTML += html;
				canvases[i].initCanvas();
			}

			initImages();
		});
	};

	var refresh = function () {
		refreshCanvases();
	};

	return {
		init: function () {
			initSideBar();
			initPageObjects();
			initTimeLine();
			initImages();
			setTimeout(function () {
				initRequests();

				if (appConfig.editor === false) {
					setInterval(function () {
						refresh();
					}, parseInt(appConfig.time_refresh) * 1000);
				}
			}, 100);

		},
		menuClick: function (index, param1, param2, param3) {
			menuClick(index, param1, param2, param3);
		},
		updateData: function (el) {
			updateData(el);
		}
	};
}();
