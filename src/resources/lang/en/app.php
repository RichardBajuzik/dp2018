<?php

return [
	'hide_menu' => 'Hide menu',
	'menu' => [
		'components' =>'Components',
		'connectors' =>'Connectors',
		'full_screen' => 'Full screen',
		'edit_mode' => 'Edit mode',
		'edit_mode_toggle' => 'Toggle mode',
		'object_add' => 'Add object',
		'settings' => 'Settings',
		'manual' => 'Manual',
		'index' => 'User screen',
		'editor' => 'Edit screen',
	],
	'time_real' => 'Real Time',
	'time_current' => 'Current Time',
];