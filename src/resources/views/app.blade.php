<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
		<title>{{ env('APP_NAME') }}</title>
		<script>
			var images = [], app = {!! json_encode($config) !!};
			function preload(a){for (var e in a)images[e] = new Image, images[e].src = a[e], images[e].alt = e}
			function ajaxGet(e, t, o, n){"function" == typeof t && (n = o, o = t), "object" == typeof e && (t = e, e = location.href); var s = "", a = 5, f = new XMLHttpRequest; if ("object" == typeof t)for (var r in t)"" !== s && (s += "&"), s += r + "=" + encodeURIComponent(t[r]); if (f.open("GET", e + (s.length?"?" + s:""), !0), "object" == typeof n)for (var r in n)f.setRequestHeader(r, n[r]); f.send(), f.onreadystatechange = function(){4 === f.readyState && 200 === f.status && "function" == typeof o && o(JSON.parse(f.responseText), f.statusText), 500 === f.status && a > 0 && (a--, setTimeout(function(){f.send()}, 1e3))}}
			function ajaxPost(e, t, o, n){"object" == typeof e && (n = o, o = t, t = e, e = location.href); var s = "", a = 5, f = new XMLHttpRequest; for (var r in t)"" !== s && (s += "&"), s += r + "=" + encodeURIComponent(t[r]); if (f.open("POST", e, !0), f.setRequestHeader("Content-type", "application/x-www-form-urlencoded"), "object" == typeof n)for (var r in n)f.setRequestHeader(r, n[r]); f.send(s), f.onreadystatechange = function(){4 === f.readyState && 200 === f.status && "function" == typeof o && o(JSON.parse(f.responseText), f.statusText), 500 === f.status && a > 0 && (a--, setTimeout(function(){f.open("POST", e, !0), f.send(s)}, 1e3))}}
			function ajaxPut(e, t, o, n){"object" == typeof e && (n = o, o = t, t = e, e = location.href); var s = "", a = 5, f = new XMLHttpRequest; for (var r in t)"" !== s && (s += "&"), s += r + "=" + encodeURIComponent(t[r]); if (f.open("PUT", e, !0), f.setRequestHeader("Content-type", "application/x-www-form-urlencoded"), "object" == typeof n)for (var r in n)f.setRequestHeader(r, n[r]); f.send(s), f.onreadystatechange = function(){4 === f.readyState && 200 === f.status && "function" == typeof o && o(JSON.parse(f.responseText), f.statusText), 500 === f.status && a > 0 && (a--, setTimeout(function(){f.open("PUT", e, !0), f.send(s)}, 1e3))}}
			function ajaxDelete(e, t, o, n){"object" == typeof e && (n = o, o = t, t = e, e = location.href); var s = "", a = 5, f = new XMLHttpRequest; for (var r in t)"" !== s && (s += "&"), s += r + "=" + encodeURIComponent(t[r]); if (f.open("DELETE", e, !0), f.setRequestHeader("Content-type", "application/x-www-form-urlencoded"), "object" == typeof n)for (var r in n)f.setRequestHeader(r, n[r]); f.send(s), f.onreadystatechange = function(){4 === f.readyState && 200 === f.status && "function" == typeof o && o(JSON.parse(f.responseText), f.statusText), 500 === f.status && a > 0 && (a--, setTimeout(function(){f.open("DELETE", e, !0), f.send(s)}, 1e3))}}
			function init() {
				ajaxPost('api/assets', {}, function (data) {
					var script = document.createElement("script");
					script.innerHTML = data.javascripts;
					document.head.appendChild(script);

					var css = document.createElement("style");
					css.type = "text/css";
					css.innerHTML = data.stylesheets;
					document.head.appendChild(css);

					preload(data.images);
					App.init();
				}, app.token);
			};
		</script>
	</head>
	<body onload="init();" class="app-body{{ $config['editor'] ? ' app-editor' : '' }}">
		<nav class="vertical_nav">

			<ul id="js-menu" class="menu">

				@if ($config['editor'] === false)
					<li class="menu--item  menu--item__has_sub_menu">

						<label class="menu--link" title="User">
							<i class="menu--icon far fa-fw fa-user"></i>
							<span class="menu--label">User</span>
						</label>

						<ul class="sub_menu">
						</ul>
					</li>

					<li class="menu--item">
						<a class="menu--link" title="{{ trans('app.menu.manual') }}">
							<i class="menu--icon fa fa-fw fa-book"></i>
							<span class="menu--label">{{ trans('app.menu.manual') }}</span>
						</a>
					</li>

					<li class="menu--item  menu--item__has_sub_menu">
						<label class="menu--link" title="{{ trans('app.menu.settings') }}">
							<i class="menu--icon fa fa-fw fa-cog"></i>
							<span class="menu--label">{{ trans('app.menu.settings') }}</span>
						</label>

						<ul class="sub_menu">
							<li class="sub_menu--item">
								<a class="sub_menu--link">Next layer</a>
							</li>
							<li class="sub_menu--item">
								<a class="sub_menu--link">Previous layer</a>
							</li>
						</ul>
					</li>
				@endif
				
				@if ($config['editor'] === true)
					<li class="menu--item menu--item__has_sub_menu">
						<label class="menu--link" title="{{ trans('app.menu.components') }}">
							<i class="menu--icon far fa-fw fa-object-ungroup"></i>
							<span class="menu--label">{{ trans('app.menu.components') }}</span>
						</label>

						<ul class="sub_menu">
							<li class="sub_menu--item sub_menu_objects">
								<div class="menu-type-object-scroll">
									@foreach ($objects as $object)
										<div aa="{{ $loop->iteration }}" class="menu-type-object {{ $loop->iteration % 5 > 0 ? 'menu-object-inline' : 'menu-object-inline' }}">
											<div class="menu-objects-item" title="{{ $object->name }}">
												<div class="menu-object-wrapper">
													<img onclick="javascript: App.menuClick('entity-place', '{{ $object->uid }}')" class="menu-object menu-object-{{ $object->uid }}" alt="{{ $object->uid }}" />
												</div>
												<div class="menu-object-label">
													{{ $object->name }}
												</div>
											</div>
										</div>
									@endforeach
								</div>
							</li>
							<li class="sub_menu--item">
								<a onclick="javascript: App.menuClick('object-add')" class="sub_menu--link">{{ trans('app.menu.object_add') }}</a>
							</li>
						</ul>
					</li>

					<li class="menu--item menu--item__has_sub_menu">
						<label class="menu--link" title="{{ trans('app.menu.connectors') }}">
							<i class="menu--icon fa fa-fw fa-arrows-alt-h"></i>
							<span class="menu--label">{{ trans('app.menu.connectors') }}</span>
						</label>

						<ul class="sub_menu">
							<li class="sub_menu--item sub_menu_objects">
								<div class="menu-type-object-scroll">
									@foreach ($connectorTypes as $connectorType)
										<div aa="{{ $loop->iteration }}" class="menu-type-connector {{ $loop->iteration % 5 > 0 ? 'menu-object-inline' : 'menu-object-inline' }}">
											<div class="menu-objects-item">
												<div class="menu-object-wrapper">
													<img onclick="javascript: App.menuClick('connector-place', '{{ $connectorType->id }}')" class="menu-object menu-object-{{ $connectorType->uid }}" alt="{{ $connectorType->uid }}" />
												</div>
												<div class="menu-object-label">
													{{ $connectorType->name }}
												</div>
											</div>
										</div>
									@endforeach
								</div>
							</li>
						</ul>
					</li>
				@endif

				@if ($config['editor'] === false)
					<li class="menu--item">
						<a onclick="javascript: App.menuClick('fullscreen')" class="menu--link" title="{{ trans('app.menu.full_screen') }}">
							<i class="menu--icon fa fa-fw fa-expand-arrows-alt"></i>
							<span class="menu--label">{{ trans('app.menu.full_screen') }}</span>
						</a>
					</li>
				@endif

				<li class="menu--item">
					@if ($config['editor'])
						<a href="{{ route('route.index') }}" class="menu--link" title="{{ trans('app.menu.index') }}">
							<i class="menu--icon fa fa-fw fa-desktop"></i>
							<span class="menu--label">{{ trans('app.menu.index') }}</span>
						</a>
					@else
						<a href="{{ route('route.editor') }}" class="menu--link" title="{{ trans('app.menu.editor') }}">
							<i class="menu--icon fa fa-fw fa-edit"></i>
							<span class="menu--label">{{ trans('app.menu.editor') }}</span>
						</a>
					@endif
				</li>
			</ul>

			<button id="collapse_menu" class="collapse_menu">
				<i class="collapse_menu--icon  fa fa-fw"></i>
				<span class="collapse_menu--label">{{ trans('app.hide_menu') }}</span>
			</button>

		</nav>

		<div class="wrapper">
			<div class="container">
				<div class="canvas">
				</div>
				@if ($config['editor'] === false)
					<div id="timeline">
						<div class="timeline-wrapper">
							<div id="timelineToolbar" class="timeline-toolbar">
								0 s<br />{{ $config['response']['time_real_min'] }}
							</div>
						</div>
					</div>

					<div id="time">
						<div class="time-wrapper">
							<div class="time-wrapper-both time-wrapper-real">
							<span>
								{{ trans('app.time_real') }}: 
							</span>
							<span id="timeReal">
								---
							</span>
						</div>
						<div class="time-wrapper-both time-wrapper-current">
							<span>
								{{ trans('app.time_current') }}: 
							</span>
							<span id="timeCurrent">
								---
							</span>
							</div>
						</div>
					</div>
				@endif
			</div>
		</div>

		<div class="modal-wrapper hidden">
			<div class="modal-box-wrapper">
				<div class="modal-box" id="modalBox">
					<form name="infoForm" method="post">
						<div id="modalContainerInfo" class="hidden">
						</div>
					</form>
					<form name="objectForm" method="post" enctype="multipart/form-data">
						<div id="modalContainerObject" class="hidden">
							<h3>Object add</h3>
							
							<table cellpadding="2" cellspacing="2" border="0" width="100%">
								<tr>
									<td>
										Name:
									</td>
									<td colspan="2">
										<input name="name" type="text" />
									</td>
								</tr>
								<tr>
									<td>
										Upload image:
									</td>
									<td colspan="2">
										<input name="object" type="file" />
									</td>
								</tr>
								<tr>
									<td>
										<strong>Constants:</strong>
									</td>
								</tr>
								<tr class="attribute-new" rel="0">
									<td>
										<input name="attrName[0]" type="text" placeholder="Attribute name" />
									</td>
									<td>
										<input name="attrValue[0]" type="text" placeholder="Attribute value" />
									</td>
									<td>
										<input name="attrRequired[0]" class="checkboxHandler" type="checkbox" title="Required attribute" />&nbsp;&nbsp;<input name="attrVisibility[0]" class="checkboxHandler" type="checkbox" title="Toggle tooltip" />
									</td>
								</tr>
								<tr>
									<td>
										Add new attribute
									</td>
									<td colspan="2">
										<img class="btn-object-add" src="assets/img/icon_plus.png" />
									</td>
								</tr>
							</table>
							<input type="hidden" name="type" value="object" />
							<input type="submit" value="Add object" />
						</div>
					</form>
					<form name="componentForm" method="post">
						<div id="modalContainerComponent" class="hidden">
							<h3>Component edit - <span id="componentName"></span></h3>
							<table cellpadding="2" cellspacing="2" border="0" width="100%">
								<tr class="attribute-param-new-constants">
								</tr>
								
								<tr class="attribute-param-new" rel="0">
									<td>
										<input name="attrName[0]" type="text" placeholder="Attribute name" />
									</td>
									<td>
										<input name="attrValue[0]" type="text" placeholder="Attribute value" />
									</td>
									<td>
										<input name="attrVisibility[0]" class="checkboxHandler" type="checkbox" title="Toggle tooltip" />
									</td>
								</tr>
								<tr>
									<td>
										Add new attribute
									</td>
									<td>
										<img class="btn-component-add" src="assets/img/icon_plus.png" />
									</td>
								</tr>
							</table>
							<input type="hidden" id="componentUid" name="uid" />
							<input type="hidden" name="type" value="component" />
							<input type="submit" value="Update component" />
						</div>
					</form>
					<form name="connectorForm" method="post">
						<div id="modalContainerConnector" class="hidden">
							<h3>Connector edit</h3>
							<table cellpadding="2" cellspacing="2" border="0" width="100%">
								<tr class="attribute-connector-new">
									<td colspan="2">
										<input name="attrName[]" type="text" placeholder="Attribute name" />&nbsp;&nbsp;<input name="attrValue[]" type="text" placeholder="Attribute value" />
									</td>
								</tr>
								<tr>
									<td>
										Add new attribute
									</td>
									<td>
										<img class="btn-connector-add" src="assets/img/icon_plus.png" />
									</td>
								</tr>
							</table>
							<input type="hidden" id="connectorId" name="id" />
							<input type="hidden" name="type" value="connector" />
							<input type="submit" value="Update connector" />
						</div>
					</form>
				</div>
			</div>
			<div class="modal-cover">
			</div>
		</div>
	</body>
</html>