<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It is a breeze. Simply tell Lumen the URIs it should respond to
  | and give it the Closure to call when that URI is requested.
  |
 */

$router->get('/', [
	'uses' => 'AppController@index',
	'as' => 'route.index'
]);

$router->get('/edit', [
	'uses' => 'AppController@editor',
	'as' => 'route.editor'
]);

$router->post('/edit', [
	'uses' => 'AppController@editor'
]);

$authType = env('AUTH_TYPE');
$middleware = empty($authType) ? null : $authType . '.auth';

$router->group(['prefix' => 'api', 'middleware' => $middleware], function () use ($router) {
	$router->post('assets', [
		'uses' => 'AppController@assets',
		'as' => 'route.assets'
	]);

	$router->post('entity', [
		'uses' => 'AppController@entityPlace',
		'as' => 'route.object-place'
	]);

	$router->put('entity', [
		'uses' => 'AppController@entityUpdate',
		'as' => 'route.entity-update'
	]);

	$router->delete('entity', [
		'uses' => 'AppController@entityDelete',
		'as' => 'route.object-place'
	]);

	$router->post('data', [
		'uses' => 'AppController@getData',
		'as' => 'route.settings'
	]);

	$router->post('connector', [
		'uses' => 'AppController@connectorPlace',
		'as' => 'route.connector-place'
	]);

	$router->delete('connector', [
		'uses' => 'AppController@connectorDelete',
		'as' => 'route.connector-delete'
	]);

	$router->put('data', [
		'uses' => 'AppController@setData',
		'as' => 'route.set-data'
	]);
});
