-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 27, 2018 at 02:16 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `diplomovka`
--

-- --------------------------------------------------------

--
-- Table structure for table `connectors`
--

DROP TABLE IF EXISTS `connectors`;
CREATE TABLE IF NOT EXISTS `connectors` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `entity_from` int(10) UNSIGNED NOT NULL,
  `entity_to` int(10) UNSIGNED NOT NULL,
  `dir_start` int(2) UNSIGNED NOT NULL,
  `dir_end` int(2) UNSIGNED NOT NULL,
  `data` longtext,
  PRIMARY KEY (`id`),
  KEY `entity_from` (`entity_from`),
  KEY `entity_to` (`entity_to`),
  KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `connectors`
--

INSERT INTO `connectors` (`id`, `type`, `entity_from`, `entity_to`, `dir_start`, `dir_end`, `data`) VALUES
(104, 2, 97, 95, 2, 1, NULL),
(106, 1, 99, 100, 2, 4, '{\"Name\":\"Conveyor #1\"}'),
(107, 1, 98, 100, 2, 1, NULL),
(108, 1, 100, 101, 8, 4, '{\"Name\":\"Conveyor #2\"}'),
(109, 3, 102, 105, 2, 8, NULL),
(110, 3, 101, 105, 2, 1, NULL),
(111, 2, 101, 102, 8, 4, NULL),
(112, 1, 104, 101, 2, 1, NULL),
(113, 1, 104, 102, 2, 1, NULL),
(114, 2, 103, 104, 2, 1, NULL),
(115, 2, 98, 104, 8, 4, NULL),
(116, 2, 95, 98, 2, 1, NULL),
(127, 2, 94, 99, 2, 1, NULL),
(128, 3, 94, 95, 8, 4, NULL),
(130, 3, 96, 106, 1, 1, NULL),
(131, 2, 97, 94, 4, 1, NULL),
(132, 2, 103, 96, 1, 2, NULL),
(133, 2, 97, 103, 2, 4, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `connector_type`
--

DROP TABLE IF EXISTS `connector_type`;
CREATE TABLE IF NOT EXISTS `connector_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(44) NOT NULL,
  `name` varchar(44) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `connector_type`
--

INSERT INTO `connector_type` (`id`, `uid`, `name`) VALUES
(1, 'conveyor', 'Conveyor'),
(2, 'pipe', 'Pipe'),
(3, 'line', 'Line');

-- --------------------------------------------------------

--
-- Table structure for table `entities`
--

DROP TABLE IF EXISTS `entities`;
CREATE TABLE IF NOT EXISTS `entities` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pos_x` int(10) UNSIGNED NOT NULL,
  `pos_y` int(10) UNSIGNED NOT NULL,
  `object_id` int(10) UNSIGNED NOT NULL,
  `layer_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `object_id` (`object_id`),
  KEY `layer_id` (`layer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `entities`
--

INSERT INTO `entities` (`id`, `pos_x`, `pos_y`, `object_id`, `layer_id`) VALUES
(74, 0, 0, 2, 1),
(94, 446, 62, 1, 1),
(95, 445, 162, 15, 1),
(96, 293, 398, 19, 1),
(97, 249, 178, 3, 1),
(98, 580, 162, 4, 1),
(99, 581, 62, 3, 1),
(100, 792, 161, 13, 1),
(101, 792, 383, 22, 1),
(102, 792, 477, 23, 1),
(103, 382, 398, 20, 1),
(104, 497, 399, 14, 1),
(105, 1017, 383, 18, 1),
(106, 297, 284, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

DROP TABLE IF EXISTS `history`;
CREATE TABLE IF NOT EXISTS `history` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `time` timestamp NOT NULL,
  `entity_id` int(11) UNSIGNED NOT NULL,
  `data` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `time` (`time`),
  KEY `entity_id` (`entity_id`)
) ENGINE=InnoDB AUTO_INCREMENT=457 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `history`
--

INSERT INTO `history` (`id`, `time`, `entity_id`, `data`) VALUES
(376, '2018-04-27 01:01:19', 94, NULL),
(377, '2018-04-27 01:01:21', 94, NULL),
(378, '2018-04-27 01:03:57', 94, NULL),
(379, '2018-04-27 01:04:10', 95, NULL),
(380, '2018-04-27 01:04:12', 95, NULL),
(381, '2018-04-27 01:04:17', 94, NULL),
(382, '2018-04-27 01:04:22', 95, NULL),
(383, '2018-04-27 01:04:41', 96, NULL),
(384, '2018-04-27 01:04:44', 96, NULL),
(385, '2018-04-27 01:04:48', 97, NULL),
(386, '2018-04-27 01:04:52', 97, NULL),
(387, '2018-04-27 01:04:59', 98, NULL),
(388, '2018-04-27 01:05:02', 98, NULL),
(389, '2018-04-27 01:05:18', 99, NULL),
(390, '2018-04-27 01:05:21', 99, NULL),
(391, '2018-04-27 01:05:23', 99, NULL),
(392, '2018-04-27 01:05:28', 100, '{\"parameters\":{\"req\":\"req\"},\"required\":[\"req\"]}'),
(393, '2018-04-27 01:05:31', 100, '{\"parameters\":{\"req\":\"req\"},\"required\":[\"req\"]}'),
(394, '2018-04-27 01:05:43', 101, NULL),
(395, '2018-04-27 01:05:46', 101, NULL),
(396, '2018-04-27 01:05:48', 102, NULL),
(397, '2018-04-27 01:05:52', 102, NULL),
(398, '2018-04-27 01:06:01', 101, NULL),
(399, '2018-04-27 01:06:04', 102, NULL),
(400, '2018-04-27 01:06:25', 103, NULL),
(401, '2018-04-27 01:06:29', 103, NULL),
(402, '2018-04-27 01:06:34', 104, NULL),
(403, '2018-04-27 01:06:40', 104, NULL),
(404, '2018-04-27 01:06:52', 104, NULL),
(405, '2018-04-27 01:08:33', 105, NULL),
(406, '2018-04-27 01:08:37', 105, NULL),
(407, '2018-04-27 01:08:51', 105, NULL),
(408, '2018-04-27 01:09:04', 102, NULL),
(409, '2018-04-27 01:09:27', 104, NULL),
(410, '2018-04-27 01:09:34', 103, NULL),
(411, '2018-04-27 01:09:46', 95, NULL),
(412, '2018-04-27 01:09:52', 95, NULL),
(413, '2018-04-27 01:09:55', 97, NULL),
(414, '2018-04-27 01:09:59', 94, NULL),
(415, '2018-04-27 01:10:00', 95, NULL),
(416, '2018-04-27 01:10:04', 94, NULL),
(417, '2018-04-27 01:10:11', 94, NULL),
(418, '2018-04-27 01:15:09', 106, NULL),
(419, '2018-04-27 01:15:12', 106, NULL),
(420, '2018-04-27 01:15:14', 96, NULL),
(421, '2018-04-27 01:15:16', 106, NULL),
(422, '2018-04-27 01:15:17', 94, NULL),
(423, '2018-04-27 01:15:39', 94, NULL),
(424, '2018-04-27 01:16:04', 94, NULL),
(425, '2018-04-27 01:16:16', 94, NULL),
(426, '2018-04-27 01:17:40', 106, NULL),
(427, '2018-04-27 01:18:01', 106, NULL),
(428, '2018-04-27 01:18:10', 106, NULL),
(429, '2018-04-27 01:18:13', 97, NULL),
(430, '2018-04-27 01:18:17', 95, NULL),
(431, '2018-04-27 01:18:20', 97, NULL),
(432, '2018-04-27 01:18:32', 97, NULL),
(433, '2018-04-27 01:19:00', 97, NULL),
(434, '2018-04-27 01:19:03', 103, NULL),
(435, '2018-04-27 01:19:06', 96, NULL),
(436, '2018-04-27 01:19:09', 106, NULL),
(437, '2018-04-27 01:19:17', 95, NULL),
(438, '2018-04-27 01:19:19', 95, NULL),
(439, '2018-04-27 01:19:23', 94, NULL),
(440, '2018-04-27 01:20:05', 100, '{\"parameters\":{\"req\":\"req\"},\"required\":[\"req\"]}'),
(445, '2018-04-27 04:08:26', 100, '{\"parameters\":{\"req\":\"req\"},\"required\":[\"req\"]}'),
(446, '2018-04-27 04:08:35', 100, '{\"parameters\":{\"req\":\"req\"},\"required\":[\"req\"]}'),
(447, '2018-04-27 04:08:44', 100, '{\"parameters\":{\"req\":\"req\"},\"required\":[\"req\"]}'),
(448, '2018-04-27 04:08:49', 100, '{\"parameters\":{\"req\":\"req\"},\"required\":[\"req\"]}'),
(449, '2018-04-27 04:08:58', 100, '{\"parameters\":{\"req\":\"req\"},\"required\":[\"req\"]}'),
(450, '2018-04-27 04:09:15', 100, '{\"parameters\":{\"req\":\"req\"},\"required\":[\"req\"]}'),
(451, '2018-04-27 04:14:53', 98, '{\"parameters\":{\"Full\":\"5 %\",\"Effectivity\":\"100 %\"},\"required\":[],\"visible\":[\"Full\"]}'),
(452, '2018-04-27 04:15:04', 97, '{\"parameters\":{\"Speed\":\"10rm\"},\"required\":[],\"visible\":[]}'),
(453, '2018-04-27 04:15:08', 97, '{\"parameters\":{\"Speed\":\"10rm\"},\"required\":[],\"visible\":[\"Speed\"]}'),
(454, '2018-04-27 04:15:34', 106, '{\"parameters\":{\"Efficiency\":\"75 %\"},\"required\":[],\"visible\":[\"Efficiency\"]}'),
(455, '2018-04-27 04:16:07', 98, '{\"parameters\":{\"Full\":\"5 %\",\"Effectivity\":\"100 %\",\"Overheat\":\"5 %\"},\"required\":[],\"visible\":[\"Full\",\"Overheat\"]}'),
(456, '2018-04-27 04:16:10', 98, '{\"parameters\":{\"Full\":\"5 %\",\"Effectivity\":\"100 %\",\"Overheat\":\"5 %\"},\"required\":[],\"visible\":[\"Full\",\"Effectivity\",\"Overheat\"]}');

-- --------------------------------------------------------

--
-- Table structure for table `layers`
--

DROP TABLE IF EXISTS `layers`;
CREATE TABLE IF NOT EXISTS `layers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(44) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `layers`
--

INSERT INTO `layers` (`id`, `name`) VALUES
(1, 'layer1');

-- --------------------------------------------------------

--
-- Table structure for table `objects`
--

DROP TABLE IF EXISTS `objects`;
CREATE TABLE IF NOT EXISTS `objects` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(44) NOT NULL,
  `uid` varchar(44) NOT NULL,
  `width` int(10) UNSIGNED NOT NULL,
  `height` int(10) UNSIGNED NOT NULL,
  `data_object` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `objects`
--

INSERT INTO `objects` (`id`, `name`, `uid`, `width`, `height`, `data_object`) VALUES
(1, 'Alkylation', 'alkylation', 64, 64, NULL),
(2, 'Breaker', 'breaker', 64, 64, NULL),
(3, 'Briquetting machine', 'briquetting_machine', 64, 64, '{\"required\":[],\"visible\":[\"Size\"],\"parameters\":{\"Size\":\"75 cm\"}}'),
(4, 'Chimney tower hyperbolic', 'chimney_tower_hyperbolic', 64, 64, '{\"required\":[],\"visible\":[\"Type\"],\"parameters\":{\"Type\":\"Steel\"}}'),
(5, 'Condenser', 'condenser', 64, 64, NULL),
(6, 'Counterflow forced draft', 'counterflow_forced_draft', 64, 64, NULL),
(7, 'Crossflow inducted draft', 'crossflow_inducted_draft', 64, 64, NULL),
(13, 'Crusher', 'crusher', 64, 64, NULL),
(14, 'Evaporative condenser', 'evaporative_condenser', 64, 64, NULL),
(15, 'Fluid catalytic cracking', 'fluid_catalytic_cracking', 64, 64, NULL),
(16, 'Fluid coking', 'fluid_coking', 64, 64, NULL),
(17, 'Fluidized reactor', 'fluidized_reactor', 64, 64, NULL),
(18, 'Forced-draft cooling tower', 'forceddraft_cooling_tower', 64, 64, NULL),
(19, 'Furnace', 'furnace', 64, 64, NULL),
(20, 'Hydrocracking', 'hydrocracking', 64, 64, NULL),
(21, 'Hydrodesulfurization', 'hydrodesulfurization', 64, 64, NULL),
(22, 'Packed tower', 'packed_tower', 64, 64, NULL),
(23, 'Plate tower', 'plate_tower', 64, 64, NULL),
(24, 'Reformer', 'reformer', 64, 64, NULL),
(25, 'Tubular', 'tubular', 64, 64, NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `connectors`
--
ALTER TABLE `connectors`
  ADD CONSTRAINT `connectors_ibfk_1` FOREIGN KEY (`entity_from`) REFERENCES `entities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `connectors_ibfk_2` FOREIGN KEY (`entity_to`) REFERENCES `entities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `connectors_ibfk_3` FOREIGN KEY (`type`) REFERENCES `connector_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `entities`
--
ALTER TABLE `entities`
  ADD CONSTRAINT `entities_ibfk_1` FOREIGN KEY (`layer_id`) REFERENCES `layers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `entities_ibfk_2` FOREIGN KEY (`object_id`) REFERENCES `objects` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `history`
--
ALTER TABLE `history`
  ADD CONSTRAINT `history_ibfk_1` FOREIGN KEY (`entity_id`) REFERENCES `entities` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
